package com.three.cabinet.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * @program: ywb_momo
 * @Author ywb(余文冰)
 * @Description:
 * @Date: Create in 14:42 2020/8/13
 * @Version: 1.0
 */

public class HttpUtil {
    //post请求
    public static String postXml(String url, String xml) {
        //1、实例化请求对象
        HttpPost post = new HttpPost(url);
//        HttpGet;
//        HttpPut;
//        HttpDelete;
        try {
            //实例化请求实体
            HttpEntity entity = new StringEntity(xml,"UTF-8");
            post.setEntity(entity);
            //实例化客户端对象
            HttpClient client = HttpClientBuilder.create().build();
            //执行请求 获取响应结果
            HttpResponse response = client.execute(post);
            //验证http状态码
            if(response.getStatusLine().getStatusCode()==200){
                //返回响应结果
                return EntityUtils.toString(response.getEntity());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
