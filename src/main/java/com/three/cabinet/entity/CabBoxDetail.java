package com.three.cabinet.entity;
import java.io.Serializable;
import lombok.Data;
import java.util.Date;

@Data
public class CabBoxDetail implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 箱子id
   */
  private Integer boxid;

  /**
   * 所属柜子id
   */
  private Integer did;

  /**
   * 箱子规格；
   */
  private String typeno;

  /**
   * 高 单位：cm
   */
  private Integer height;

  /**
   * 深 单位：cm
   */
  private Integer deep;

  /**
   * 宽 单位：cm
   */
  private Integer wide;

  /**
   * 规格状态，a表示启用，s表示禁用
   */
  private String status;

  /**
   * 添加时间
   */
  private Date addtime;

  /**
   * 修改时间
   */
  private Date updatetime;

  public CabBoxDetail() {
  }

}
