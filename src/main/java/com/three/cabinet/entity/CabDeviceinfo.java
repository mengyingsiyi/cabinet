package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import java.util.Date;

@Data
public class CabDeviceinfo implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 设备id
   */
  private Integer did;

  /**
   * 设备唯一标识号
   */
  private String identity;

  /**
   * 设备在线状态；on表示在线；off表示离线
   */
  private String linestate;

  /**
   * 设备mac
   */
  private String mac;

  /**
   * ip
   */
  private String dip;

  /**
   * duid
   */
  private Integer duid;

  /**
   * 设备名称
   */
  private String name;

  /**
   * 经度
   */
  private Double longitude;

  /**
   * 纬度
   */
  private Double latitude;

  /**
   * 设备柜子类型 ;one表示只有一种箱子，two表示大小两种箱子，three表示大中小三种箱子。
   */
  private String type;

  /**
   * fix表示固定版本，flow表示流动版本
   */
  private String usetypeno;

  /**
   * 设备收费类型 ; 付款方式；zfb表示支付宝，wx表示微信，pp表示paypal，cash表示现金
   */
  private String payment;

  /**
   * 总收入
   */
  private Double totalincome;

  /**
   * 微信总收入
   */
  private Double wxtotalincome;

  /**
   * 支付宝总收入
   */
  private Double zfbtotalincome;

  /**
   * 广告位到期时间
   */
  private Date adexpiretime;

  /**
   * 运维人员id
   */
  private Integer wsorkeruserid;

  /**
   * 厂商id
   */
  private Integer manufactuserid;

  public CabDeviceinfo() {
  }

}
