package com.three.cabinet.controller;

import com.three.cabinet.dto.AliPayRefundDto;
import com.three.cabinet.dto.PayDto;
import com.three.cabinet.service.PayService;
import com.three.cabinet.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @program:
 * @Author
 * @Description:
 * @Date: Create in 21:00 2020/8/13
 * @Version: 1.0
 */
@Api(tags = "订单支付相关")
@RestController
@RequestMapping("/api/pay/")
public class PayController {
    @Autowired
    private PayService service;

    @ApiOperation(value = "生成付款二维码", notes = "生成付款二维码")
    @PostMapping("sendpay")
    public R<String> createPay(@RequestBody PayDto dto) {
        System.err.println(dto);
        return service.createPay(dto);
    }

    @ApiOperation(value = "查看订单的支付状态", notes = "查看订单的支付状态")
    @GetMapping("querypay/{oid}")
    public R queryPay(@PathVariable String oid) {
        return service.queryPay(oid);
    }
    @ApiOperation(value = "关闭一个未付款的订单", notes = "关闭一个未付款的订单")
    @PostMapping("closepay/{oid}")
    public R refundPay(@PathVariable String oid) {
        return service.closePay(oid);
    }
    @ApiOperation(value = "只支持阿里退款", notes = "只支持阿里退款")
    @PostMapping("refound")
    public R refund(AliPayRefundDto refundDto) {
        return service.refound(refundDto);
    }
}
