package com.three.cabinet.exception;

/**
 * 自定义异常
 */
public class UserException extends Exception{
    public UserException(){
        super();
    }
    public UserException(String msg){
        super(msg);
    }
}
