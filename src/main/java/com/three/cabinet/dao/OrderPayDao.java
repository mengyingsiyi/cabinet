package com.three.cabinet.dao;


import com.three.cabinet.dto.OrderPay;
import com.three.cabinet.entity.CabOrderPay;
import org.apache.ibatis.annotations.Insert;

/**
 * @program:
 * @Author
 * @Description:
 * @Date: Create in 20:34 2020/8/13
 * @Version: 1.0
 */

public interface OrderPayDao {
    int insert(OrderPay pay);

    OrderPay selectByOid(String oid);

    CabOrderPay selectByUid(int uid);

    //修改订单状态
    int updateFlag();

    int lottoLog(Integer userId, String productName,String oid);
}
