package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import java.util.Date;

@Data
public class CabBoxUseRecord implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 记录id
   */
  private Integer burid;

  /**
   * 箱子id
   */
  private Integer biid;

  /**
   * 归属柜子
   */
  private Integer did;

  /**
   * 使用价格，单位分
   */
  private Integer price;

  /**
   * 付款方式；zfb表示支付宝，wx表示微信
   */
  private String payment;

  /**
   * 存放时长：单位分钟
   */
  private Integer duration;

  /**
   * 开始存放时间
   */
  private Date starttime;

  /**
   * 结束存放时间
   */
  private Date endtime;

  /**
   * 添加时间
   */
  private Date addtime;

  public CabBoxUseRecord() {
  }

}
