package com.three.cabinet.configuration;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.three.cabinet.dao.TakeNoticeDao;
import com.three.cabinet.dto.SendNoticeDto;
import com.three.cabinet.entity.CabNatification;
import com.three.cabinet.service.TakeNoticeService;
import com.three.cabinet.util.JedisCore;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

/**
 * @Package: com.three.cabinet.configuration
 * @ClassName: DelayReceiver
 * @Author: yan
 * @Description:
 */
@Component
public class DelayReceiver {
    @Autowired
    private JedisCore jedisCore;
    @Autowired
    private TakeNoticeService service;

    @RabbitListener(queues = {DelayRabbitmqConfiguration.NOTICE_CODE_QUEUE_NAME})
    public void noticeDelayQueue(SendNoticeDto sendNoticeDto, Message message, Channel channel){
        if (jedisCore.checkKey(sendNoticeDto.getNcid()+":"+sendNoticeDto.getVerificationCode())){
            service.againSendNotice(sendNoticeDto);
        }
    }
}
