package com.three.cabinet.dto;

import lombok.Data;

import java.util.Date;

/**
 * @Package: com.three.cabinet.dto
 * @ClassName: SendNoticeDto
 * @Author: yan
 * @Description:
 */
@Data
public class SendNoticeDto {

    /**
     * 通知短信
     */
    private Integer ncid;

    /**
     * 短信内容
     */
    private String content;

    /**
     * 手机号
     */
    private String telephone;

    /**
     * 验证码
     */
    private String verificationCode;


    public SendNoticeDto(String notice, String name, int code, String telephone) {

    }
}
