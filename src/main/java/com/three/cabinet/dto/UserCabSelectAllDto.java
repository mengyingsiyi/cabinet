package com.three.cabinet.dto;

import lombok.Data;

@Data
public class UserCabSelectAllDto {
        /**
         * 真实姓名
         */
        private String username;

        /**
         * 设备名称
         */
        private String name;
        /**
         * 视频名称
         */
        private String videoName;
        /**
         * 视频简介
         */
        private String videoIntroduce;
        /**
         * 视频地址 //视频地址通过oss
         */
        private String videoUrl;
        /**
         * 超时时间
         */
        private String expirationTime;
        /**
         * 状态 a其余 s禁用
         */
        private String state;

}
