package com.three.cabinet.dao;


import com.three.cabinet.dto.UserCabSelectAllDto;
import com.three.cabinet.entity.CabAdvertisement;
import com.three.cabinet.entity.CabDeviceAdvertisement;
import com.three.cabinet.vo.R;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 广告信息
 */
public interface CabAdvertisementDao {


    int update(CabAdvertisement cabAdvertisement);

    int delete(int id);

    CabAdvertisement findById(int id);

    List<CabAdvertisement> findAllList();

    int upload(CabAdvertisement cabAdvertisement);

}
