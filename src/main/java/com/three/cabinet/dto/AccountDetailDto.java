package com.three.cabinet.dto;

import com.three.cabinet.Enum.AccountDetailFlag;
import lombok.Data;

import java.util.Date;

/**
 * @program: cabinet
 * @Author
 * @Description:
 * @Date: Create in 16:12 2020/8/20
 * @Version: 1.0
 */
@Data
public class AccountDetailDto {
    /**
     * 用户id
     */
    private int uid;

    /**
     * 变动原因
     */
    private AccountDetailFlag type;

    /**
     * 变更的金额
     */
    private double money;

    /**
     * 变动的时间
     */
    private Date atime;

}
