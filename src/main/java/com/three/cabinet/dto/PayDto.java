package com.three.cabinet.dto;

import lombok.Data;

/**
 * @program:
 * @Author
 * @Description: 
 * @Date: Create in 13:38 2020/8/13 
 * @Version: 1.0
 */
@Data
public class PayDto {
    private Integer uid;
    private String oid;//订单号
    private int price;//单位分
    private String orderdes;//订单的描述信息
    private int paytype;//支付方式 1 支付宝 2微信
}
