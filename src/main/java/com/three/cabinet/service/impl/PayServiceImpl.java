package com.three.cabinet.service.impl;

import com.alibaba.fastjson.JSON;
import com.three.cabinet.config.RedisKeyConfig;
import com.three.cabinet.dao.OrderPayDao;
import com.three.cabinet.dao.PayDao;
import com.three.cabinet.dto.AliPayDto;
import com.three.cabinet.dto.AliPayRefundDto;
import com.three.cabinet.dto.OrderPay;
import com.three.cabinet.dto.PayDto;
import com.three.cabinet.service.PayService;
import com.three.cabinet.util.AliPayUtil;
import com.three.cabinet.util.JedisCore;
import com.three.cabinet.util.WxPayUtil;
import com.three.cabinet.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Base64;

/**
 * @program:
 * @Author
 * @Description: 
 * @Date: Create in 20:42 2020/8/13 
 * @Version: 1.0
 */
@Service
public class PayServiceImpl implements PayService {

    @Autowired
    private OrderPayDao dao;
    @Autowired
    private PayDao payDao;
    @Autowired
    private JedisCore jedisCore;
    @Autowired
    private OrderPayDao orderPayDao;

    @Override
    public R<String> createPay(PayDto dto) {
        if(dto.getPaytype()<3) {
            AliPayDto aliPayDto=new AliPayDto();
            String qrcodeurl=null;
            if (dto.getPaytype() == 1) {
                //支付宝
                aliPayDto.setOut_trade_no(dto.getOid());
                aliPayDto.setSubject(dto.getOrderdes());
                aliPayDto.setTotal_amount(dto.getPrice()/100.0);
                System.err.println(aliPayDto);
                qrcodeurl= AliPayUtil.createPayUrl(JSON.toJSONString(aliPayDto));
            } else if (dto.getPaytype() == 2) {
                //微信
                qrcodeurl= WxPayUtil.wxpay_create(dto);
            }
            if(!StringUtils.isEmpty(qrcodeurl)){
                //拥有支付链接
                String url="http://localhost:8088/api/qrcode/payqrcode/"+ Base64.getUrlEncoder().encodeToString(qrcodeurl.getBytes());
                OrderPay pay=new OrderPay();
                pay.setBody(dto.getOrderdes());
                pay.setOid(dto.getOid());
                pay.setPayurl(url);
                pay.setUid(dto.getUid());
                pay.setPaymoney(dto.getPrice());
                pay.setType(dto.getPaytype());
                dao.insert(pay);
                System.err.println(url);
                return R.ok(url);
            }
            return R.error("亲，第三方支付服务不可用！");
        }else {
            return R.error("亲，你选择的支付方式目前还不支持！");
        }
    }

    @Override
    public R<String> queryPay(String oid) {
        OrderPay pay=dao.selectByOid(oid);
        if(pay!=null){
            if(pay.getFlag()==1){
                //未支付
                String r,msg=null;
                if(pay.getType()==1){
                    //支付宝
                    r=AliPayUtil.queryPay(oid);
                }else {
                    //微信支付
                    r=WxPayUtil.wxpay_query(oid);
                }
                if(!StringUtils.isEmpty(r)) {
                    switch (r) {
                        case "WAIT_BUYER_PAY":
                        case "NOTPAY":
                            msg = "未付款";
                            break;
                        case "TRADE_CLOSED":
                        case "CLOSED":
                        case "REVOKED":
                            msg = "交易超时关闭";
                            break;
                        case "TRADE_SUCCESS":
                        case "SUCCESS":
                            msg = "支付成功";
                            //记录账户变动记录
                            OrderPay orderPay = dao.selectByOid(oid);
                            Integer uid = orderPay.getUid();
                            Integer type = orderPay.getType();
                            Double money= null;
                            if(type == 1){
                                money = Integer.parseInt(jedisCore.get(RedisKeyConfig.ACCOUNTADDMOEY + oid))/100.0;
                            }
                            if(uid!=null){
                                payDao.editMoney(money,uid);
                            }
                            //修改订单状态
                            orderPayDao.updateFlag();
                            break;
                        case "TRADE_FINISHED":
                            msg = "交易结束";
                            break;
                        default:msg="异常状态";break;
                    }
                }
                return R.ok(msg);
            }
            return R.error("请稍后重试");
        }else {
            return R.error("亲，请检查订单号！");
        }
    }

    @Override
    public R<String> closePay(String oid) {
        OrderPay pay=dao.selectByOid(oid);
        if(pay!=null){
            if(pay.getFlag()==1){
                //关闭订单
                String r;
                if(pay.getType()==1){
                    r=AliPayUtil.closePay(oid);
                }else {
                    r=WxPayUtil.wxpay_close(oid);
                }
                if(StringUtils.isEmpty(r)){
                    return R.error("关闭订单失败！");
                }else {
                    return R.ok();
                }
            }else {
                return R.error("亲，无法关闭订单，请检查订单状态");
            }
        }else {
            return R.error("亲，请检查订单号！");
        }
    }

    @Override
    public R refound(AliPayRefundDto refundDto) {
        boolean b = AliPayUtil.refundPay(JSON.toJSONString(refundDto));
        if(b){
            return R.ok();
        }
        return R.error("稍后再试");
    }
}
