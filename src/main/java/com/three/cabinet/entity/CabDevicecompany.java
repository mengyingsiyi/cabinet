package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;

@Data
public class CabDevicecompany implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * dcid
   */
  private Integer dcid;

  /**
   * 公司id
   */
  private Integer comid;

  /**
   * 设备id
   */
  private Integer did;

  public CabDevicecompany() {
  }

}
