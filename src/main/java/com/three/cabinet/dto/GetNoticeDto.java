package com.three.cabinet.dto;

import lombok.Data;

/**
 * @Package: com.three.cabinet.dto
 * @ClassName: GetNoticeDto
 * @Author: yan
 * @Description:
 */
@Data
public class GetNoticeDto {
    private String telephone;
    private Integer tracknum;
    private Integer deviceId;
}
