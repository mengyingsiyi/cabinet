package com.three.cabinet.service.impl;

import com.three.cabinet.dao.SignInDao;
import com.three.cabinet.dao.UserDao;
import com.three.cabinet.dto.SignInDto;
import com.three.cabinet.entity.CabSignIn;
import com.three.cabinet.entity.CabUser;
import com.three.cabinet.service.SignService;
import com.three.cabinet.util.DateUtil;
import com.three.cabinet.util.JedisCore;
import com.three.cabinet.util.StringUtil;
import com.three.cabinet.util.TokenUtil;
import com.three.cabinet.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.three.cabinet.util.DateUtil.formatDate;

@Service
public class SignServiceImpl implements SignService {
    @Autowired
    private JedisCore jedisCore;
    @Autowired
    private SignInDao signInDao;
    @Autowired
    private UserDao userDao;

    @Override
    public R doSign(int uid, LocalDate localDate) {
        CabUser user =  userDao.selectUserById(uid);
        if (user!=null){//用户存在
            SignInDto signIn = signInDao.selectSignIn(uid);
            Map<String, Object> detailMap = new HashMap<>();
            //系统当前日期
            //Date date = DateUtil.localDateToDate(localDate);
            Date date = DateUtil.localDateTime2Date(LocalDateTime.now());
            String s2 = DateUtil.dateFormate(date,"yyyy-MM-dd");
            if (signIn!=null){//不是首次签到，已有签到记录 -> 修改记录
                detailMap = StringUtil.getStringToMap(signIn.getDetail());
                //相差天数
                String s1 = DateUtil.dateFormate(signIn.getUpdateTime(),"yyyy-MM-dd");
                int days = (int) DateUtil.betweenStrDate(s1,s2);
                //当前连续签到天数
                int conDays = signIn.getContinueDays();
                //当前积分
                int score = signIn.getIntegral();
                if (days == 0) {
                    return R.ok("您今天已经签到了，不要重复签到哦！");
                } else if (days == 1) {//可签到
                    //增加积分
                    if (conDays >= 7) {//连续签到7天
                        //连续签到7天及以上，积分+7
                        score += 7;
                        signIn.setIntegral(score);
                    } else if (conDays < 7) {//未连续签满7天
                        //签到未满7天，签到第几天增加多少积分
                        score += conDays + 1;
                        signIn.setIntegral(score);
                    }
                    //签到天数+1
                    signIn.setContinueDays(conDays + 1);
                    signIn.setUpdateTime(date);
                    detailMap.put(s2,1);
                    signIn.setDetail(StringUtil.getMapToString(detailMap));
                    if (signInDao.updateSignIn(signIn)) {//签到
                        int offset = localDate.getDayOfMonth()-1;
                        boolean flag = jedisCore.setBit(TokenUtil.buildSignKey(uid, localDate), offset, true);
                        if (flag){
                            return R.ok("您已签到");
                        }else {
                            return R.ok("签到成功！");
                        }
                    } else {
                        return R.error("签到失败，请稍后重试!");
                    }
                } else {//断签
                    signIn.setContinueDays(1);
                    signIn.setIntegral(1);
                    signIn.setUpdateTime(date);
                    detailMap.put(s2,1);
                    signIn.setDetail(StringUtil.getMapToString(detailMap));
                    if (signInDao.updateSignIn(signIn)) {//签到
                        int offset = localDate.getDayOfMonth()-1;
                        boolean flag = jedisCore.setBit(TokenUtil.buildSignKey(uid, localDate), offset, true);
                        if (flag){
                            return R.ok("您已签到");
                        }else {
                            return R.ok("签到成功！");
                        }
                    } else {
                        return R.error("签到失败，请稍后重试!");
                    }
                }
            }else {//首次签到 -> 插入记录
                CabSignIn newSignIn = new CabSignIn();
                newSignIn.setUserId(uid);
                //记录签到日期
                detailMap.put(s2,1);
                String s = StringUtil.getMapToString(detailMap);
                newSignIn.setDetail(s);
                newSignIn.setUpdateTime(date);
                if (newSignIn!=null){
                    if (signInDao.insertSignIn(newSignIn)){
                        int offset = localDate.getDayOfMonth()-1;
                        jedisCore.setBit(TokenUtil.buildSignKey(uid, localDate), offset, true);
                        return R.ok("签到成功！");
                    }else {
                        return R.error("签到失败，请稍后重试!");
                    }
                }else {
                    return R.error("签到记录不能为空");
                }
            }
        }else {
            return R.error("用户不存在!请注册后重试!");
        }

    }

    @Override
    public R checkSign(int uid, LocalDate date) {
        CabUser user =  userDao.selectUserById(uid);
        if (user!=null) {//用户存在
            int offset = date.getDayOfMonth() - 1;
            boolean flag = jedisCore.getBit(TokenUtil.buildSignKey(uid, date), offset);
            if (flag) {
                return R.ok("您今天已经签到了，不要重复签到哦！");
            }else {
                return R.ok("您还未签到哦！");
            }
        }else {
            return R.error("您还未注册哦!请注册后再签到!");
        }

    }

    @Override
    public R getSignCount(int uid, LocalDate date) {
        CabUser user =  userDao.selectUserById(uid);
        if (user!=null) {//用户存在
            long l = jedisCore.bitCount(TokenUtil.buildSignKey(uid, date));
            if (l>0) {
                return R.ok(l);
            }else {
                return R.error("您还未签到哦！");
            }
        }else {
            return R.error("您还未注册哦!没有签到记录!");
        }
    }

    @Override
    public R getContinuousSignCount(int uid, LocalDate date) {
        CabUser user =  userDao.selectUserById(uid);
        if (user!=null) {//用户存在
            int signCount = 0;
            String type = String.format("u%d", date.getDayOfMonth());
            List<Long> list = jedisCore.bitFiled(TokenUtil.buildSignKey(uid,date), "GET", type, "0");
            if (list != null && list.size() > 0) {
                // 取低位连续不为0的个数即为连续签到次数，需考虑当天尚未签到的情况
                long v = list.get(0) == null ? 0 : list.get(0);
                for (int i = 0; i < date.getDayOfMonth(); i++) {
                    if (v >> 1 << 1 == v) {
                        // 低位为0且非当天说明连续签到中断了
                        if (i > 0) break;
                    } else {
                        signCount += 1;
                    }
                    v >>= 1;
                }
            }
            return R.ok(signCount);
        }else {
            return R.error("您还未注册哦!没有签到记录!");
        }
    }

    @Override
    public R getFirstSignDate(int uid, LocalDate date) {
        CabUser user =  userDao.selectUserById(uid);
        if (user!=null) {//用户存在
            long pos = jedisCore.bitPos(TokenUtil.buildSignKey(uid, date), true);
            if (pos<0) {
                return R.error("您还未签到哦！");
            }else {
                return R.ok(date.withDayOfMonth((int) (pos + 1)));
            }
        }else {
            return R.error("您还未注册哦!没有签到记录!");
        }
    }

    @Override
    public R getSignInfo(int uid, LocalDate date) {
        CabUser user =  userDao.selectUserById(uid);
        if (user!=null) {//用户存在
            Map<String, Boolean> signMap = new HashMap<>(date.getDayOfMonth());
            String type = String.format("u%d", date.lengthOfMonth());
            List<Long> list = jedisCore.bitFiled(TokenUtil.buildSignKey(uid, date), "GET", type, "0");
            if (list != null && list.size() > 0) {
                // 由低位到高位，为0表示未签，为1表示已签
                long v = list.get(0) == null ? 0 : list.get(0);
                for (int i = date.lengthOfMonth(); i > 0; i--) {
                    LocalDate d = date.withDayOfMonth(i);
                    signMap.put(formatDate(d, "yyyy-MM-dd"), v >> 1 << 1 != v);
                    v >>= 1;
                }
            }
            return R.ok(signMap);
        }else {
            return R.error("您还未注册哦!没有签到记录!");
        }
    }
}
