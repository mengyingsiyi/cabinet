package com.three.cabinet.dto;

import lombok.Data;

import java.util.Date;

/**
 * @program:
 * @Author
 * @Description:
 * @Date: Create in 20:35 2020/8/13
 * @Version: 1.0
 */
@Data
public class OrderPay {
    private int uid;
    private String oid;
    private String body;
    private Integer paymoney;
    private Integer type;
    private Integer flag;
    private String payurl;
    private Date ctime;

}
