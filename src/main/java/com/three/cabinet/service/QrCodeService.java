package com.three.cabinet.service;

import javax.servlet.http.HttpServletResponse;

/**
 * @program: ywb_momo
 * @Author ywb(余文冰)
 * @Description: 
 * @Date: Create in 20:41 2020/8/12 
 * @Version: 1.0
 */

public interface QrCodeService {
    //明文生成
    void createQrcode(String msg, HttpServletResponse response);
    //密文
    void createQrcodePass(String msg, HttpServletResponse response);
}
