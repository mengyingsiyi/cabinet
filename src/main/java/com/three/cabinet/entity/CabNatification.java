package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CabNatification implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 通知短信
   */
  private Integer ncid;

  /**
   * 短信内容
   */
  private String content;

  /**
   * 手机号
   */
  private String telephone;

  /**
   * 验证码
   */
  private String verificationCode;

  /**
   * 添加人
   */
  private Integer adduserid;

  /**
   * 添加时间
   */
  private Date addtime;

  /**
   * 设备名称
   */
  private String name;


  public CabNatification() {
  }

}
