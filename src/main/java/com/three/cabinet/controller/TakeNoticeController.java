package com.three.cabinet.controller;

import com.three.cabinet.dto.GetNoticeDto;
import com.three.cabinet.service.TakeNoticeService;
import com.three.cabinet.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Package: com.three.cabinet.controller
 * @ClassName: TakeNoticeController
 * @Author: yan
 * @Description:
 */
@Api("通知模块")
@RestController
public class TakeNoticeController {
    @Autowired
    private TakeNoticeService takeNoticeService;
    @ApiOperation("短信通知")
    public R sendNotice(@RequestBody GetNoticeDto getNoticeDto){
        return takeNoticeService.sendNotice(getNoticeDto);
    }
}
