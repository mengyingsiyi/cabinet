package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;

@Data
public class CabRole implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * id
   */
  private Integer id;

  /**
   * 角色
   */
  private String role;

  /**
   * 权限
   */
  private Integer permission;

  public CabRole() {
  }

}
