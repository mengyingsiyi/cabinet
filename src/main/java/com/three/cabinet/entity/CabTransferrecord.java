package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import java.util.Date;

@Data
public class CabTransferrecord implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 交易流水号
   */
  private String id;

  /**
   * 转账id
   */
  private String trid;

  /**
   * 归属用户
   */
  private String userid;

  /**
   * 归属柜子
   */
  private Integer did;

  /**
   * 费用(分)
   */
  private Integer cost;

  /**
   * 手续费
   */
  private Integer poundage;

  /**
   * 状态s:表示成功  f表示失败
   */
  private String statusno;

  /**
   * nn添加时间
   */
  private Date addtime;

  /**
   * 更新时间
   */
  private Date updatetime;

  public CabTransferrecord() {
  }

}
