package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;

@Data
public class CabUser implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * id
   */
  private Integer id;

  /**
   * 真实姓名
   */
  private String username;

  /**
   * 性别，1代表女生，2代表男生
   */
  private Integer sex;

  /**
   * 身份证号
   */
  private String idcard;

  /**
   * 手机号
   */
  private String telphone;

  /**
   * 密码
   */
  private String password;

  /**
   * 角色，1表示普通用户，2表示快递员，3表示广告商
   */
  private Integer roleid;

  /**
   * 账户状态，1表示启用，2表示禁用
   */
  private Integer status;

  public CabUser() {
  }

}
