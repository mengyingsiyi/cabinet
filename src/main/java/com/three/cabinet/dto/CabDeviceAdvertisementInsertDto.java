package com.three.cabinet.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CabDeviceAdvertisementInsertDto {

    /**
     * 柜组id
     */
    private int groupId;
    /**
     * 广告id
     */
    private int adId;



}
