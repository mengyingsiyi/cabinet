package com.three.cabinet.controller;

import com.three.cabinet.dto.PayDto;
import com.three.cabinet.service.AccountService;
import com.three.cabinet.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @program: cabinet
 * @Author ywb(余文冰)
 * @Description:
 * @Date: Create in 21:44 2020/8/20
 * @Version: 1.0
 */
@RestController
@RequestMapping("/api/user/pay")
@Api(tags = "用户账户相关")
public class UserAccountController {
    @Autowired
    private AccountService service;

    /**
     * @param money 充值的金额
     * @param type 充值的方式 1为支付宝 2为微信
     * @param request
     * @return
     */
    @ApiOperation(value = "充值", notes = "充值")
    @GetMapping("insert")
    public R insertBalance(int money, int type, HttpServletRequest request) {
        request.setAttribute("userId",new Integer(1));
        return service.addBalance(money, type,(Integer) request.getAttribute("userId"));
    }
    @ApiOperation(value = "满足寄件两次抽奖", notes = "满足寄件两次抽奖")
    @GetMapping("lotto")
    public R lotto(HttpServletRequest request){
        request.setAttribute("userId",new Integer(1));
        return service.lotto((Integer) request.getAttribute("userId"));
    }
}
