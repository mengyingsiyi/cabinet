package com.three.cabinet.service.impl;

import com.three.cabinet.Enum.AccountDetailFlag;
import com.three.cabinet.config.JedisConfig;
import com.three.cabinet.config.RedisKeyConfig;
import com.three.cabinet.dao.OrderPayDao;
import com.three.cabinet.dto.LottoDto;
import com.three.cabinet.dto.PayDto;
import com.three.cabinet.entity.CabOrderPay;
import com.three.cabinet.service.AccountService;
import com.three.cabinet.service.PayService;
import com.three.cabinet.util.JedisCore;
import com.three.cabinet.util.LottoUtil;
import com.three.cabinet.util.UUIDUtil;
import com.three.cabinet.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @program: cabinet
 * @Author 苗帅奇
 * @Description:
 * @Date: Create in 18:03 2020/8/20
 * @Version: 1.0
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private PayService payService;
    @Autowired
    private OrderPayDao dao;
    @Autowired
    private JedisCore jedisCore;

    /**
     * 充值
     *
     * @return
     */
    @Override
    public R addBalance(int money, int type, Integer uid) {
        System.out.println(uid);
        PayDto payDto = new PayDto();
        payDto.setPrice(money);
        payDto.setOrderdes(AccountDetailFlag.充值 + "");
        String uuid = UUIDUtil.createUUID();
        payDto.setOid(uuid);
        payDto.setPaytype(type);
        payDto.setUid(uid);
        //查询未支付的订单
        CabOrderPay cabOrderPay = dao.selectByUid(uid);
        if (cabOrderPay != null) {
            return R.error("你有未提交充值的订单,请先取消或充值");
        }
        jedisCore.set(RedisKeyConfig.ACCOUNTADDMOEY + uuid, "" + money, RedisKeyConfig.ACCOUNTPAYTYPE_TIME);
        return payService.createPay(payDto);

    }

    @Override
    public R lotto(Integer userId) {
        LottoDto iphone = new LottoDto();
        iphone.setId(101);
        iphone.setProductName("苹果手机");
        iphone.setProb(0.0005D);

        LottoDto thanks = new LottoDto();
        thanks.setId(102);
        thanks.setProductName("再接再厉");
        thanks.setProb(0.2D);

        LottoDto tenIntegral = new LottoDto();
        tenIntegral.setId(103);
        tenIntegral.setProductName("10积分");
        tenIntegral.setProb(0.2D);

        LottoDto fitthIntegral = new LottoDto();
        fitthIntegral.setId(104);
        fitthIntegral.setProductName("15积分");
        fitthIntegral.setProb(0.2D);

        LottoDto vip = new LottoDto();
        vip.setId(105);
        vip.setProductName("优酷会员");
        vip.setProb(0.1D);

        LottoDto cap = new LottoDto();
        cap.setId(106);
        cap.setProductName("水杯");
        cap.setProb(0.1D);

        List<LottoDto> list = new ArrayList<>();
        list.add(vip);
        list.add(thanks);
        list.add(iphone);
        list.add(fitthIntegral);
        list.add(tenIntegral);
        list.add(cap);


        int index = LottoUtil.drawGift(list);
        System.out.println(list.get(index));
        if(!(list.get(index).getProductName().equals("再接再厉"))){
            dao.lottoLog(userId,list.get(index).getProductName(),UUIDUtil.createUUID());
        }
        return R.ok(list.get(index).getProductName());
    }
}
