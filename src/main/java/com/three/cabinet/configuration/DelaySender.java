package com.three.cabinet.configuration;

import com.three.cabinet.dto.SendNoticeDto;
import com.three.cabinet.entity.CabNatification;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Package: com.three.cabinet.configuration
 * @ClassName: DelaySender
 * @Author: yan
 * @Description:
 */
@Component
public class DelaySender {
    @Autowired
    private AmqpTemplate amqpTemplate;
    public void sendDelay(SendNoticeDto sendNoticeDto){
        this.amqpTemplate.convertAndSend(DelayRabbitmqConfiguration.NOTICE_CODE_DELAY_EXCHANGE,DelayRabbitmqConfiguration.NOTICE_CODE_DELAY_ROUTING_KEY,sendNoticeDto,message -> {
            // 如果配置了 params.put("x-message-ttl", 5 * 1000); 那么这一句也可以省略,具体根据业务需要是声明 Queue 的时候就指定好延迟时间还是在发送自己控制时间
            message.getMessageProperties().setExpiration(60*60*24+"");
            return message;
        });
    }
}
