package com.three.cabinet.Enum;

/**
 * @program: cabinet
 * @Author
 * @Description:    账户变动的状态
 * @Date: Create in 16:13 2020/8/20
 * @Version: 1.0
 */

public enum AccountDetailFlag {
    提现("ext"), 充值("topup"), 收入("take"), 支出("exp"), 提现返回("back");
    private String flag;

    public String getVal() {
        return flag;
    }

    private AccountDetailFlag(String f) {
        flag = f;
    }
}
