package com.three.cabinet.dto;

import lombok.Data;

/**
 * @program: ywb_momo
 * @Author ywb(余文冰)
 * @Description: 
 * @Date: Create in 20:35 2020/8/12 
 * @Version: 1.0
 */

@Data
public class AliPayRefundDto {
//    订单支付时传入的商户订单号,不能和 trade_no同时为空
    private String out_trade_no;
//    需要退款的金额，该金额不能大于订单金额,单位为元，支持两位小数
    private double refund_amount;
//    标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传
    private String out_request_no;
}
