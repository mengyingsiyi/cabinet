package com.three.cabinet.dto;

import lombok.Data;
import lombok.ToString;

/**
 * @program: cabinet
 * @Author ywb(余文冰)
 * @Description: 中奖的记录日志
 * @Date: Create in 20:15 2020/8/22 
 * @Version: 1.0
 */
@Data
@ToString
public class LottoDto {
    private Integer id;//奖品id
    private String productName;
    private double prob;

}
