package com.three.cabinet.util;

/**
 * @program: cabinet
 * @Author ywb(余文冰)
 * @Description:
 * @Date: Create in 20:22 2020/8/22
 * @Version: 1.0
 */

import com.three.cabinet.dto.LottoDto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 抽奖工具类
 * 整体思想：
 * 奖品集合 + 概率比例集合
 * 将奖品按集合中顺序概率计算成所占比例区间，放入比例集合。并产生一个随机数加入其中，排序。</br>
 * 排序后，随机数落在哪个区间，就表示那个区间的奖品被抽中。
 * 返回的随机数在集合中的索引，该索引就是奖品集合中的索引。
 * 比例区间的计算通过概率相加获得。
 *
 */
public class LottoUtil {
    public static int drawGift(List<LottoDto> giftList){
        if(null != giftList && giftList.size()>0){
            List<Double> orgProbList = new ArrayList<Double>(giftList.size());
            for(LottoDto gift:giftList){
                //按顺序将概率添加到集合中
                orgProbList.add(gift.getProb());
            }

            return draw(orgProbList);

        }
        return -1;
    }
    public static int draw(List<Double> giftProbList){

        List<Double> sortRateList = new ArrayList<Double>();

        // 计算概率总和
        Double sumRate = 0D;
        for(Double prob : giftProbList){
            sumRate += prob;
        }

        if(sumRate != 0){
            double rate = 0D;   //概率所占比例
            for(Double prob : giftProbList){
                rate += prob;
                // 构建一个比例区段组成的集合(避免概率和不为1)
                sortRateList.add(rate / sumRate);
            }

            // 随机生成一个随机数，并排序
            double random = Math.random();
            sortRateList.add(random);
            Collections.sort(sortRateList);

            // 返回该随机数在比例集合中的索引
            return sortRateList.indexOf(random);
        }


        return -1;
    }
    public static void main(String[] args) {
        LottoDto iphone = new LottoDto();
        iphone.setId(101);
        iphone.setProductName("苹果手机");
        iphone.setProb(0.1D);

        LottoDto thanks = new LottoDto();
        thanks.setId(102);
        thanks.setProductName("再接再厉");
        thanks.setProb(0.5D);
        LottoDto cap = new LottoDto();
        thanks.setId(108);
        thanks.setProductName("水杯");
        thanks.setProb(0.5D);

        LottoDto vip = new LottoDto();
        vip.setId(103);
        vip.setProductName("优酷会员");
        vip.setProb(0.4D);

        List<LottoDto> list = new ArrayList<LottoDto>();
        list.add(vip);
        list.add(thanks);
        list.add(iphone);

        for(int i=0;i<100;i++){
            int index = drawGift(list);
            System.out.println(list.get(index));
        }
    }
}
