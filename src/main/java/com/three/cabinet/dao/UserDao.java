package com.three.cabinet.dao;

import com.three.cabinet.entity.CabSenditems;
import com.three.cabinet.entity.CabUser;

import java.util.List;

public interface UserDao {
    //寄件
    boolean insertOrder(CabSenditems senditems);

    //查询用户寄件次数
    int orderNum(Integer uid);

    //查询订单
    List<CabSenditems> selectOrder(Integer uid);

    //查询用户
    CabUser selectUserById(Integer uid);
}
