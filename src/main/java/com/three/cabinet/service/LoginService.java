package com.three.cabinet.service;

import com.three.cabinet.dto.LoginDto;
import com.three.cabinet.entity.CabUser;
import com.three.cabinet.vo.R;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: cabinet
 * @description:
 * @author: ztk
 * @create: 2020-08-20 17:58
 **/
public interface LoginService {

    // 登录校验
    CabUser findUser(LoginDto dto);

    CabUser findUserByTelPhone(String telPhone);

    // 登录
    R loginInDevice(LoginDto dto, HttpServletRequest request);

    // 二维码登录快递柜，仅快递员
    R loginByQRInCabinet(String telPhone);

    // 二维码登录pc，普通用户和快递员
    R loginByQRInPC(String telPhone, HttpServletRequest request);

    // 手机验证码登录
    R loginByTelMsg();

    // 主动请求刷新令牌
    
}
