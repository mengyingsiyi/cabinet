package com.three.cabinet.entity;

import java.io.Serializable;

import lombok.Data;
import java.util.Date;

@Data
public class CabSignIn implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 主键
   */
  private Integer id;

  /**
   * 签到用户id
   */
  private Integer userId;

  /**
   * 连续签到天数
   */
  private Integer continueDays;

  /**
   * 更新日期， 最后签到日期
   */
  private Date updateTime;

  /**
   * 用户积分
   */
  private Integer integral;

  /**
   * 签到详情
   */
  private String detail;

  public CabSignIn() {
  }

}
