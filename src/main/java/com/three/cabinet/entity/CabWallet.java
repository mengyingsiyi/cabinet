package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;

@Data
public class CabWallet implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * id
   */
  private Integer id;

  /**
   * 用户编号
   */
  private Integer userid;

  /**
   * 收入
   */
  private Double income;

  /**
   * 余额
   */
  private Double balance;

  public CabWallet() {
  }

}
