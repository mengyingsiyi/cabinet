package com.three.cabinet.entity;
import java.io.Serializable;
import lombok.Data;
import java.util.Date;


@Data
public class CabAccountdetail implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * nn账户id就是用户id
   */
  private Integer uid;

  /**
   * 账户(卡号)
   */
  private String accid;

  /**
   * nn提现:ext 充值:topup 收入:take 支出:exp 提现返回: back
   */
  private String type;

  /**
   * 分配收益来源id  referid
   */
  private String referid;

  /**
   * nn变动的时间
   */
  private Date atime;

  public CabAccountdetail() {
  }

}
