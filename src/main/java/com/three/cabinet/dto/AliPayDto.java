package com.three.cabinet.dto;

import lombok.Data;

/**
 * @program:
 * @Author
 * @Description: 
 * @Date: Create in 20:34 2020/8/12 
 * @Version: 1.0
 */
@Data
public class AliPayDto {
    private String out_trade_no;
    private double total_amount;
    private String subject;
}
