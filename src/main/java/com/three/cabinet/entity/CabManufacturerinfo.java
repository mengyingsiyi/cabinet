package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;

@Data
public class CabManufacturerinfo implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 设备厂商id
   */
  private Integer comid;

  /**
   * 公司名称
   */
  private String cmpyname;

  /**
   * 公司地址
   */
  private String cmpyaddress;

  public CabManufacturerinfo() {
  }

}
