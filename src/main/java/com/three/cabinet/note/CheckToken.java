package com.three.cabinet.note;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @program: cabinet
 * @description:用于校验令牌的注解
 * @author:
 * @create: 2020-08-22 20:16
 **/
@Target(value = ElementType.METHOD)//为属性添加的注解
@Retention(value = RetentionPolicy.RUNTIME)//配置运行时
public @interface CheckToken {
    boolean required() default true;
}
