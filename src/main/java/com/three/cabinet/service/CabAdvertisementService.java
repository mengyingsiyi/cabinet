package com.three.cabinet.service;


import com.three.cabinet.dto.CabAdvertisementUpdateDto;
import com.three.cabinet.vo.R;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 广告信息
 * @author: miao
 * @date: 2020/8/20
 */
public interface CabAdvertisementService {
    /**
     * 新增
     */
/*    R insert(CabAdvertisementInsertDto dto,MultipartFile file);*/

    /**
     * 上传视频
     */
    R upload(MultipartFile file, String brief,HttpServletRequest request);


    /**
     * 删除
     */
    R delete(int id);

    /**
     * 更新
     */
    R update(CabAdvertisementUpdateDto dto, HttpServletRequest request);

    /**
     * 根据主键 id 查询
     */
    R load(int id);

}
