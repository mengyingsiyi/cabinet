package com.three.cabinet.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.crypto.Data;
import java.util.Date;
import java.util.UUID;

/**
 * @program: OpenMain
 * @description:
 * @author: Jerry(姜源)
 * @create: 2020-07-30 16:43
 */
public class JwtUtil {

    /**
     * 以JWT算法生成密文
     * @param msg 需要加密的字符串
     */
    public static String createJWT(String msg) {
        //实例化 建造器对象
        JwtBuilder jwtBuilder = Jwts.builder();
        //设置内容信息
        jwtBuilder.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        jwtBuilder.setIssuedAt(new Date());
        jwtBuilder.setSubject(msg);
        // jwtBuilder.setExpiration() //设置结束时间
        //设置加密的方式
        jwtBuilder.signWith(SignatureAlgorithm.HS256, createKey());
        //生成密文
        return jwtBuilder.compact();
    }
    public static String createJWT(String msg, Date date) {
        //实例化 建造器对象
        JwtBuilder jwtBuilder = Jwts.builder();
        //设置内容信息
        jwtBuilder.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        jwtBuilder.setIssuedAt(new Date());
        jwtBuilder.setSubject(msg);
        //设置结束时间
        jwtBuilder.setExpiration(date);
        //设置加密的方式
        jwtBuilder.signWith(SignatureAlgorithm.HS256, createKey());
        //生成密文
        return jwtBuilder.compact();
    }
    public static String createJWT(String msg, Date date, String id) {
        //实例化 建造器对象
        JwtBuilder jwtBuilder = Jwts.builder();
        //设置内容信息
        jwtBuilder.setId(id);
        jwtBuilder.setIssuedAt(new Date());
        jwtBuilder.setSubject(msg);
        //设置结束时间
        jwtBuilder.setExpiration(date);
        //设置加密的方式
        jwtBuilder.signWith(SignatureAlgorithm.HS256, createKey());
        //生成密文
        return jwtBuilder.compact();
    }
    public static String createJWT(String msg, String id) {
        //实例化 建造器对象
        JwtBuilder jwtBuilder = Jwts.builder();
        //设置内容信息
        jwtBuilder.setId(id);
        jwtBuilder.setIssuedAt(new Date());
        jwtBuilder.setSubject(msg);
        //设置加密的方式
        jwtBuilder.signWith(SignatureAlgorithm.HS256, createKey());
        //生成密文
        return jwtBuilder.compact();
    }

    /**
     * 解析JWT生成的密文
     */
    public static String parseJWT(String msg) {
        return Jwts.parser().setSigningKey(createKey()).parseClaimsJws(msg).getBody().getSubject();
    }

    /**
     * 判断jwt是否过期
     * @param jwt
     * @return
     */
    public static boolean isJWTOverdue(String jwt) {
        Date overTime = Jwts.parser()
                .setSigningKey(createKey())
                .parseClaimsJws(jwt)
                .getBody()
                .getExpiration();
        return overTime.getTime() > new Date().getTime() ? true : false;
    }

    /**
     * 获得jwt的签发时间
     * @param jwt
     * @return
     */
    public static long getJWTIssuanceTime(String jwt) {
        return Jwts.parser()
                .setSigningKey(createKey())
                .parseClaimsJws(jwt)
                .getBody()
                .getIssuedAt().getTime();
    }

    /**
     * 生成秘钥
     */
    private static SecretKey createKey() {
        String key = "wangba00";
        SecretKey secretKey = new SecretKeySpec(key.getBytes(), "AES");
        return secretKey;
    }
}
