package com.three.cabinet.entity;

import lombok.Data;

import java.util.Date;

/**
 * @program: cabinet
 * @Author ywb(余文冰)
 * @Description: 
 * @Date: Create in 20:29 2020/8/21 
 * @Version: 1.0
 */
@Data
public class CabOrderPay {
    private Integer uid;
    private String oid;
    private String body;
    private int type;
    private int flag;
    private Date ctime;
    private String payurl;
}
