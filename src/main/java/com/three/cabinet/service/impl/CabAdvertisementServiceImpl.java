package com.three.cabinet.service.impl;

import com.alibaba.fastjson.JSON;
import com.three.cabinet.config.JedisConfig;
import com.three.cabinet.config.RedisKeyConfig;
import com.three.cabinet.constant.SystemConstant;
import com.three.cabinet.dao.CabAdvertisementDao;

import com.three.cabinet.dto.CabAdvertisementUpdateDto;
import com.three.cabinet.dto.OssNameAndUrl;
import com.three.cabinet.entity.CabAdvertisement;
import com.three.cabinet.entity.CabUser;
import com.three.cabinet.note.CheckToken;
import com.three.cabinet.service.CabAdvertisementService;
import com.three.cabinet.util.FileUtil;
import com.three.cabinet.util.JedisCore;
import com.three.cabinet.util.OssSingleCore;
import com.three.cabinet.vo.R;
import org.apache.catalina.User;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;


/**
 * 广告信息
 * @author: miao
 * @date: 2020/8/20
 */
@Service
public class CabAdvertisementServiceImpl implements CabAdvertisementService {
    @Autowired
    private CabAdvertisementDao dao;


    /**
     * 上传视频
     * @param file
     * @return
     */
    @Override
    @CheckToken
    public R upload(MultipartFile file, String brief,HttpServletRequest request) {
        Object userId = request.getSession().getAttribute("userId");
        if (userId == null) {
            return R.error("userId 为空");
        } else {
            try {


                OssNameAndUrl ossNameAndUrl = uploadUtil(file);
                if (!StringUtils.isEmpty(ossNameAndUrl)) {

                    //赋值
                    CabAdvertisement ca = new CabAdvertisement();
                    ca.setVideoName(ossNameAndUrl.getVideoName());
                    ca.setVideoUrl(ossNameAndUrl.getVideoUrl());
                    ca.setVideoIntroduce(brief);
                    ca.setUserId((int)userId);
                    if (dao.upload(ca) > 0) {
                        System.err.println("上传成功");
                        return R.ok();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return R.error("oss上传失败，请检查上传的内容");

        }



    }

    /**
     * 上传工具 仅在本类使用
     * @param file
     * @return 地址 和 名称
     */
    private OssNameAndUrl uploadUtil(MultipartFile file) throws IOException {
        System.err.println(file);
        //非空判断
        if (!file.isEmpty()) {

                //获取上传内容
                byte[] bytes = file.getBytes();
                //视频审核是否违法
                //重命名、限制长度
                String rename = FileUtil.rename(file.getOriginalFilename());
                //上传
            boolean b = OssSingleCore.getInstance().checkBucket(SystemConstant.OSS_BUCKET);
            System.out.println(b);
            System.err.println(rename);
                String url = OssSingleCore.getInstance().upload(SystemConstant.OSS_BUCKET, rename, file.getBytes());
            System.err.println(url);
                if (url != null || !"".equals(url)) {
                    OssNameAndUrl ca = new OssNameAndUrl();
                    ca.setVideoName(rename);
                    ca.setVideoUrl(url);
                    return ca;
                }
        }
        return null;
    }

    /**
     * 通过id删除信息
     * @param id
     * @param
     * @return
     */
    @Override
    @CheckToken
    public R delete(int id) {
            int delete = dao.delete(id);
            if (delete > 0) {
                return R.ok();
            } else {
                return R.error("删除失败");
            }
    }

    /**
     * 修改广告信息
     * @param dto 要修改的信息
     * @param
     * @return
     */
    @Override
    @CheckToken
    public R update(CabAdvertisementUpdateDto dto, HttpServletRequest request) {

        Object userId = request.getSession().getAttribute("userId");
        if (userId == null) {
            return R.error("userId 为空");
        } else {

            CabAdvertisement ca = new CabAdvertisement();
            System.err.println(dto.getAdId());
            ca.setAdId(dto.getAdId());
            ca.setUserId((int)userId);
            ca.setVideoIntroduce(dto.getVideoIntroduce());

            int update = dao.update(ca);

            if (update > 0) {
                return R.ok();
            } else {
                return R.error("修改失败");
            }
        }

    }

    /**
     * 查看广告
     * @param id
     * @param
     * @return
     */
    @Override
    @CheckToken
    public R load(int id) {

            CabAdvertisement byId = dao.findById(id);
            if (byId != null) {
                return R.ok(byId);
            } else {

                return R.error("未找到");
            }
    }


}
