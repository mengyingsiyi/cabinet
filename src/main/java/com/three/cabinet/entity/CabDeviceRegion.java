package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import java.util.Date;

@Data
public class CabDeviceRegion implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 地址id
   */
  private Integer addressid;

  /**
   * 省
   */
  private String province;

  /**
   * 城市
   */
  private String city;

  /**
   * 区名称
   */
  private String area;

  /**
   * 具体位置(string)
   */
  private String location;

  /**
   * 具体位置(什么第三方的定位的好用的)
   */
  private String locationcode;

  /**
   * 添加时间
   */
  private Date addtime;

  /**
   * 更新时间
   */
  private Date updatetime;

  public CabDeviceRegion() {
  }

}
