package com.three.cabinet.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @Package: com.three.cabinet.dao
 * @ClassName: TakeNoticeDao
 * @Author: yan
 * @Description:
 */
public interface TakeNoticeDao {
    @Select("select name from cab_deviceinfo where dId=#{deviceId} limit 1")
    String getDeviceById(int deviceId);

    @Insert("insert into cab_natification(content,addtime,name,verificationcode,telephone) values (#{notice} ,now(),#{name} ,#{code} ,#{telephone} )")
    int addNotice(@Param("notice") String notice,@Param("name") String name, @Param("code")int code, @Param("telephone")String telephone);
}
