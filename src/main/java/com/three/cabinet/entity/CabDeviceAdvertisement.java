package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import java.util.List;

@Data
public class CabDeviceAdvertisement implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * device_ad_id
   */
  private Integer deviceAdId;

  /**
   * 柜组id
   */
  private Integer groupId;

  /**
   * 广告id
   */
  private Integer adId;

  /**
   * 状态: a其余 s禁用
   */
  private String state;

  /**
   * 超时时间
   */
  private int expirationTime;

  /**
   * 添加时间
   */
  private Date addTime;

  /**
   * 更新时间
   */
  private Date updateTime;

  private List<CabAdvertisement> cabAdvertisementList;
  private List<CabDeviceinfo> deviceinfoList;


  public CabDeviceAdvertisement() {
  }

}
