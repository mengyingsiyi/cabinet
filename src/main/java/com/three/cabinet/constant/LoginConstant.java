package com.three.cabinet.constant;

/**
 * @program: cabinet
 * @description: 其实所有都应该用
 * @author: ztk
 * @create: 2020-08-23 16:35
 **/
public class LoginConstant {

    // 用户令牌(USER_TOKEN:IP:telephoneNumber)
    public static final String USER_TOKEN = "USER_TOKEN:";
    // 快递员用户令牌(COURIER_TOKEN:telephoneNumber),仅在柜子上使用
    public static final String COURIER_TOKEN = "COURIER_TOKEN:";
    public static final int USER_TOKEN_TIME = -1;

    // 登录令牌
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final int ACCESS_TOKEN_TIME = 2 * 60 * 60 * 1000;
    public static final String COURIER_ACCESS_TOKEN = "COURIER_ACCESS_TOKEN";
    public static final int COURIER_ACCESS_TOKEN_TIME = 15 * 60 * 1000;

    // 刷新令牌
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    public static final int REFRESH_TOKEN_TIME = 30 * 24 * 60 * 60 * 1000;

    // 短信验证令牌(MSG_CODE:USERID:IP)
    public static final String MSG_CODE = "MSG_CODE:";

    // 错误令牌(ERROR_TOKEN:USERID:)
    // 按天来算，错误三次就frozen
    public static final String ERROR_TOKEN = "REFRESH_TOKEN:";

    // 冻结令牌
    public static final String FROZEN_TOKEN = "FROZEN_TOKEN";
    public static final int FROZEN_TOKEN_TIME = 12 * 60 * 60;

    // 用户已登录
    public static final String USER_ALREADY_LOGIN_CODE = "20001";
    // 用户正常登录
    public static final String USER_NORMAL_LOGIN_CODE = "10000";

    // 快递员登录
    public static final String COURIER_LOGIN_CODE = "00000";
}
