package com.three.cabinet.note;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @program: cabinet
 * @description:用于校验快递柜快递员令牌的注解
 * @author:
 * @create: 2020-08-24 00:51
 **/
@Target(value = ElementType.METHOD)//为属性添加的注解
@Retention(value = RetentionPolicy.RUNTIME)//配置运行时
public @interface CheckCourierToken {
    boolean required() default true;
}
