package com.three.cabinet.dto;

import java.util.Date;

public class SendItemsDto {
    //private int appid;//用户id
    private int sdaid;//发地址ID
    private int rdaid;//收地址ID
    private String payClass;//付款方式：到付,寄付
    private int goodsCount;//物品数量
    private String memo;//备注
    private String sendcode;//寄件码
    private String takecode;//取件码
    private int cfid;//快递费
    private int num;//箱子编号
    private int did;//柜子
    private String trackNum;//快递单号
    private int courierId;//快递员
    private String orderNum;//订单号
    private String payment;//收费类型：支付宝，微信
    private String transferStatus;//转账状态
    private String orderState;//订单状态
    private Date addTime;//添加时间
    private Date updatetime;//更新时间
}
