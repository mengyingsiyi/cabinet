package com.three.cabinet.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;
import java.util.Set;

/**
 * @program: springboot-01
 * @description:封装jedis中的一些常用的方法
 * @author:
 * @create: 2020-07-15 15:09
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JedisCore {

    private Jedis jedis;

    //构造方法
    public JedisCore(String host,int port,String pass){
        JedisPool pool=new JedisPool(host,port);
        jedis=pool.getResource();
        jedis.auth(pass);
    }
    //新增
    public void set(String key,String val,int seconds){
        if (seconds == -1) {
            jedis.set(key, val);
        } else {
            jedis.setex(key,seconds,val);
        }
    }
    //删除
    public void del(String key){
        jedis.del(key);
    }
    //查询
    public String get(String key){
        return jedis.get(key);
    }

    //系统，判断key是否存在，用于登录校验，可以用于限制唯一登录
    public boolean checkKey(String key){
        return jedis.exists(key);
    }

    //查看剩余的时间：这里用它来向用户展示冻结账号还有多久可以重新登陆
    public long ttl(String key){
        return jedis.ttl(key);
    }
    //用于匹配相似键的多少，
    public int keys(String key){
        return jedis.keys(key).size();
    }
    //设置键的有效期，可以用来设置用户登录的令牌和用户信息的有效期，用来限制唯一登陆和超过登录时间自动失效
    public void expire(String key,int seconds){
        jedis.expire(key,seconds);
    }


    /**
     * 用keys命令查出所有key，然后删除（自己弄的可能有问题）
     * @param keyStar
     * @return
     */
    public synchronized boolean delAllKeyFromKeysSelect(String keyStar) {
        Set<String> keys = jedis.keys(keyStar);
        Long del = 0l;
        for (String key : keys)
            del += jedis.del(key);
        return del > 0 ? true : false;
    }

    // 模糊查询
    public Set<String> getKeys(String keys){
        return jedis.keys(keys);
    }


    //设置key或清除指定偏移量上的位
    public boolean setBit(String key,int offset,boolean value){
        return jedis.setbit(key,offset,value);
    }

    //获取指定偏移量上的位
    public boolean getBit(String key,int offset){
        return jedis.getbit(key,offset);
    }

    //计算设置为1的数量
    public long bitCount(String key) {
        return jedis.bitcount(key);
    }

    //返回二进制位组成的数组
    public List<Long> bitFiled(String key, String... arg){
        return jedis.bitfield(key,arg);
    }

    //返回位图中第一个值为 bit 的二进制位的位置
    public long bitPos(String key, boolean value){
        return jedis.bitpos(key,value);
    }
}
