package com.three.cabinet.service;

import com.three.cabinet.dto.GetNoticeDto;
import com.three.cabinet.dto.SendNoticeDto;
import com.three.cabinet.vo.R;

/**
 * @Package: com.three.cabinet.service
 * @ClassName: TakeNoticeService
 * @Author: yan
 * @Description:
 */
public interface TakeNoticeService {
    R sendNotice(GetNoticeDto getNoticeDto);
    R againSendNotice(SendNoticeDto sendNoticeDto);
}
