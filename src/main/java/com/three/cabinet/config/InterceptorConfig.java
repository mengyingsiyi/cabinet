package com.three.cabinet.config;

import com.three.cabinet.interceptor.*;
import com.three.cabinet.util.JedisCore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer{
    //此处注入JedisCore
    @Autowired
    private JedisCore jedisCore;

    @Autowired
    private TokenInterceptor tokenInterceptor;
    @Autowired
    private AuthorizationInterceptor authorizationInterceptor;
    @Autowired
    private DeviceInterceptor deviceInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /*
            1.加入的顺序就是拦截器执行的顺序，
            2.按顺序执行所有拦截器的preHandle
            3.所有的preHandle 执行完再执行全部postHandle 最后是postHandle
         */
        // 过滤令牌
        registry.addInterceptor(tokenInterceptor)
                .addPathPatterns("/api/**");
        // 过滤快递柜快递员令牌
        registry.addInterceptor(deviceInterceptor)
                .addPathPatterns("api/cabinet/**");
        // 权限校验，普通用户的接口有令牌就可以直接访问
        registry.addInterceptor(authorizationInterceptor)
                .addPathPatterns("/api/cabinet/**")
                .excludePathPatterns("/api/cabinet/user/**");
    }

}
