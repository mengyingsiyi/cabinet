package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;

@Data
public class CabTakeitems implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 取件单记录
   */
  private Integer tiid;

  /**
   * 收件人手机号
   */
  private String telphone;

  /**
   * 收件人地址id
   */
  private Integer rdaid;

  /**
   * 快递单号
   */
  private Integer tracknum;

  /**
   * 快递员id
   */
  private Integer courierid;

  /**
   * 柜子id
   */
  private Integer did;

  /**
   * 箱号
   */
  private Integer num;

  /**
   * 金额
   */
  private Double price;

  /**
   * 快递状态 fg:待收货；js:结束；hs:回收；
   */
  private String courierstatus;

  /**
   * 取件码
   */
  private String takecode;

  public CabTakeitems() {
  }

}
