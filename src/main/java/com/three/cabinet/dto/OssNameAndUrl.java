package com.three.cabinet.dto;

import lombok.Data;

@Data
public class OssNameAndUrl {
    /**
     * 视频名称
     */
    private String videoName;

    /**
     * 视频地址 //视频地址通过oss
     */
    private String videoUrl;
}
