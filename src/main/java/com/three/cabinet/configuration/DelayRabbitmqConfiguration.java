package com.three.cabinet.configuration;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Package: com.three.cabinet.configuration
 * @ClassName: DelayRabbitmqConfig
 * @Author: yan
 * @Description:
 */
@Configuration
public class DelayRabbitmqConfiguration {
    /*
    * 延迟队列TTL名称
    * */
    private static final String NOTICE_CODE_DELAY_QUEUE="notice.delay.queue";

    /*
    * DLX,dead letter发送到的exchange
    * 延迟消息就是发送到该交换机的
    * */
    public static final String NOTICE_CODE_DELAY_EXCHANGE="notice.delay.exchange";

    /*
    * routing key 名称 路由键
    * 具体消息发送在该routingkeyde
    * */
    public static final String NOTICE_CODE_DELAY_ROUTING_KEY="notice.delay";

    //队列名
    public static final String NOTICE_CODE_QUEUE_NAME = "notice.queue";
    //交换机
    public static final String NOTICE_CODE_EXCHANGE_NAME = "notice.exchange";
    //路由
    public static final String NOTICE_CODE_ROUTING_KEY = "notice";

    /*
    * 死信接受队列
    * */
    @Bean
    public Queue noticeQueue(){
        return new Queue(NOTICE_CODE_QUEUE_NAME,true);
    }

    /*
    * 死信交换机
    * 将路由键和某模式进行匹配，此时队列需要绑定到一个模式上
    * 符号#匹配一个或多个词  符号*匹配不多不少的一个词
    * 因此“audit.#”能够匹配到“audit.irs.corporate”，但是“audit.*” 只会匹配到“audit.irs”。
    * */
    @Bean
    public TopicExchange noticeTopicExchange(){
        return new TopicExchange(NOTICE_CODE_EXCHANGE_NAME);
    }

    /*
    * 死信接受队列绑定交换机
    * */
    @Bean
    public Binding noticeBinding(){
        //如果让延迟队列之间有关联，这里的routingkey和绑定的交换机很关键
        return BindingBuilder.bind(noticeQueue()).to(noticeTopicExchange()).with(NOTICE_CODE_ROUTING_KEY);
    }

    /*
    * 死信队列的配置
    * 1.param.put("x-message-ttl",5*1000);
    * 第一种是直接设置queue的延迟时间，但如果直接给队列设置过期时间，这种做法不是很灵活，(当然二者是兼容的,默认是时间小的优先）
    * 2.rabbitTemplate.convertAndSend(book,message ->{
    * message.getMessageProperties().setExporation(2*1000+"");
    * return message;
    * });
    * 第二种是每次发送消息动态设置延迟时间，这样我们可以灵活控制
    * */
    @Bean
    public Queue delayNoticeQueue(){
        Map<String,Object> params = new HashMap<>();
        //x-dead-letter-exchange 声明了队列里的死信转发到DLX名称
        params.put("x-dead-letter-exchange",NOTICE_CODE_EXCHANGE_NAME);
        //x-dead-letter-routing-key 声明了这些死信在转发时携带的 routing-key 名称。
        params.put("x-dead-letter-routing-key",NOTICE_CODE_ROUTING_KEY);
        return new Queue(NOTICE_CODE_DELAY_QUEUE,true,false,false,params);
    }

    /**
     * 需要将一个队列绑定到交换机上，要求该消息与一个特定的路由键完全匹配。
     * 这是一个完整的匹配。如果一个队列绑定到该交换机上要求路由键 “dog”，则只有被标记为“dog”的消息才被转发，
     * 不会转发dog.puppy，也不会转发dog.guard，只会转发dog。
     * @return DirectExchange
     */
    @Bean
    public DirectExchange noticeDelayExchange() {
        return new DirectExchange(NOTICE_CODE_DELAY_EXCHANGE);
    }

    /**
     * 延迟队列绑定交换机
     */
    @Bean
    public Binding dlxBinding() {
        return BindingBuilder.bind(delayNoticeQueue()).to(noticeDelayExchange()).with(NOTICE_CODE_DELAY_ROUTING_KEY);
    }
}
