package com.three.cabinet.interceptor;

import com.three.cabinet.constant.LoginConstant;
import com.three.cabinet.entity.CabUser;
import com.three.cabinet.exception.UserException;
import com.three.cabinet.note.CheckCourierToken;
import com.three.cabinet.note.CheckToken;
import com.three.cabinet.util.IpUtil;
import com.three.cabinet.util.JacksonUtil;
import com.three.cabinet.util.JedisCore;
import com.three.cabinet.util.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @program: cabinet
 * @description:
 * @author: ztk
 * @create: 2020-08-24 00:40
 **/
@Component
public class DeviceInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        //检查是否有CheckCourierToken注释，有没则跳过认证
        if (method.isAnnotationPresent(CheckCourierToken.class)) {
            CheckCourierToken courierToken = method.getAnnotation(CheckCourierToken.class);
            if (courierToken.required()) {
                String userToken = request.getHeader(LoginConstant.ACCESS_TOKEN);
                if (!StringUtils.isEmpty(userToken)) {
                    try {
                        // 仅用校验jwt是否过期
                        JwtUtil.parseJWT(userToken);
                    } catch (ExpiredJwtException e) {
                        throw new UserException("快递柜快递员需要重新登录！");
                    }
                    return true;
                } else {
                    throw new UserException("没有登录令牌！");
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
