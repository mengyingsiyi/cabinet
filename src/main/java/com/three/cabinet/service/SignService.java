package com.three.cabinet.service;

import com.three.cabinet.vo.R;

import java.time.LocalDate;

public interface SignService {

    //用户签到
    R doSign(int uid, LocalDate date);

    //检查用户是否签到
    R checkSign(int uid, LocalDate date);

    //获取当月签到次数
    R getSignCount(int uid, LocalDate date);

    //获取当月连续签到次数
    R getContinuousSignCount(int uid, LocalDate date);

    //获取当月首次签到日期
    R getFirstSignDate(int uid, LocalDate date);

    //获取当月签到情况
    R getSignInfo(int uid, LocalDate date);
}
