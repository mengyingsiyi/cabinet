package com.three.cabinet.service;

import com.three.cabinet.dto.AliPayRefundDto;
import com.three.cabinet.dto.PayDto;
import com.three.cabinet.vo.R;

/**
 * @program: ywb_momo
 * @Author ywb(余文冰)
 * @Description:
 * @Date: Create in 20:41 2020/8/13
 * @Version: 1.0
 */

public interface PayService {
    //生成支付信息，返回支付二维码
    R<String> createPay(PayDto dto);
    //查询支付状态
    R<String> queryPay(String oid);
    //关闭订单
    R<String> closePay(String oid);
    //退款
    R refound(AliPayRefundDto refundDto);
}
