package com.three.cabinet.dto;

import lombok.Data;

/**
 * @program: cabinet
 * @description:
 * @author: ztk
 * @create: 2020-08-21 00:04
 **/
@Data
public class LoginDto {

    private String telPhone;

    private String password;
}
