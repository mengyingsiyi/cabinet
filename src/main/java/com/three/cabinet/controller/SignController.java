package com.three.cabinet.controller;

import com.three.cabinet.service.SignService;
import com.three.cabinet.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@Api(tags = "用户签到接口")
@RestController
@RequestMapping("/api/cabinet/sign")
public class SignController {
    @Autowired
    private SignService signService;

    //用户签到
    @ApiOperation(value = "用户签到")
    @PostMapping("doSign.do")
    public R doSign(int uid, LocalDate date){
        return signService.doSign(uid,date);
    }

    //检查用户是否签到
    @ApiOperation(value = "检查用户是否签到")
    @PostMapping("checkSign.do")
    public R checkSign(int uid, LocalDate date){
        return signService.checkSign(uid,date);
    }

    //获取当月签到次数
    @ApiOperation(value = "获取当月签到次数")
    @PostMapping("getSignCount.do")
    public R getSignCount(int uid, LocalDate date){
        return signService.getSignCount(uid,date);
    }

    //获取当月连续签到次数
    @ApiOperation(value = "获取当月连续签到次数")
    @PostMapping("getContinuousSignCount.do")
    public R getContinuousSignCount(int uid, LocalDate date){
        return signService.getContinuousSignCount(uid,date);
    }

    //获取当月首次签到日期
    @ApiOperation(value = "获取当月首次签到日期")
    @PostMapping("getFirstSignDate.do")
    public R getFirstSignDate(int uid, LocalDate date){
        return signService.getFirstSignDate(uid,date);
    }

    //获取当月签到情况
    @ApiOperation(value = "获取当月签到情况")
    @PostMapping("getSignInfo.do")
    public R getSignInfo(int uid, LocalDate date){
        return signService.getSignInfo(uid,date);
    }

}
