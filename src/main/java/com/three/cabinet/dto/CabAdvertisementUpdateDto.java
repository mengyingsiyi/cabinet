package com.three.cabinet.dto;

import lombok.Data;

@Data
public class CabAdvertisementUpdateDto {

        /**
         * ad_id
         */
        private int adId;


        /**
         * 视频名称
         */
//        private String videoName;
        /**
         * 视频简介
         */
        private String videoIntroduce;
        /**
         * 视频地址 //视频地址通过oss
         */
//        private String videoUrl;//从oss中获取


}
