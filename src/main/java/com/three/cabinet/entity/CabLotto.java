package com.three.cabinet.entity;

import lombok.Data;

/**
 * @program: cabinet
 * @Author ywb(余文冰)
 * @Description: 抽奖的奖品
 * @Date: Create in 20:13 2020/8/22 
 * @Version: 1.0
 */
@Data
public class CabLotto {
    private int id;//奖品id
    /**
     * 奖品名字
     */
    private String name;
    private int count;
}
