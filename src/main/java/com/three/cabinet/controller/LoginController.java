package com.three.cabinet.controller;

import com.three.cabinet.dto.LoginDto;
import com.three.cabinet.service.LoginService;
import com.three.cabinet.vo.R;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: cabinet
 * @description:
 * @author: ztk
 * @create: 2020-08-21 00:02
 **/
@Api(tags = "登录模块")
@RestController
@RequestMapping("/api/cabinet")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public R loginInDevice(@RequestBody LoginDto loginDto, HttpServletRequest request) {
        return loginService.loginInDevice(loginDto, request);
    }

    @GetMapping("/cabinet/qrloginb/{telPhone}")
    public R loginByQRInCabinet(@PathVariable String telPhone) {
        return loginService.loginByQRInCabinet(telPhone);
    }

    @GetMapping("/cabinet/qrlogina/{telPhone}")
    public R loginByQRInPC(@PathVariable String telPhone, HttpServletRequest request) {
        return loginService.loginByQRInPC(telPhone, request);
    }

    // @GetMapping("")
    // public R bbb() {
    //     return loginService.loginByTelMsg();
    // }
}
