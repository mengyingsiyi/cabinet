package com.three.cabinet.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignInDto {
    private int userId;
    private int continueDays;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")//后台到前台的时间格式的转换
    private Date updateTime;
    private int integral;
    private String detail;

}
