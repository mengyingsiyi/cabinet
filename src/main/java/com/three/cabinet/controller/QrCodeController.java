package com.three.cabinet.controller;


import com.three.cabinet.service.QrCodeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * @program: ywb_momo
 * @Author ywb(余文冰)
 * @Description:
 * @Date: Create in 20:40 2020/8/12
 * @Version: 1.0
 */

@RestController
@RequestMapping("/api/qrcode")
public class QrCodeController {

    @Autowired
    private QrCodeService service;

    @GetMapping("/showqrcode/{msg}")
    public void showQrCode(@PathVariable String msg, HttpServletResponse response) {
        service.createQrcode(msg, response);
    }

    @GetMapping("/payqrcode/{msg}")
    public void payQrCode(@PathVariable String msg, HttpServletResponse response) {
        service.createQrcodePass(msg, response);
    }
}
