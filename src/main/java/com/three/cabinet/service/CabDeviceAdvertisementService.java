package com.three.cabinet.service;

import com.three.cabinet.dto.CabDeviceAdvertisementInsertDto;
import com.three.cabinet.dto.CabDeviceAdvertisementUpdateDto;
import com.three.cabinet.entity.CabDeviceAdvertisement;
import com.three.cabinet.vo.R;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 设备广告信息
 * @author: miao
 * @date: 2020/8/20
 */
public interface CabDeviceAdvertisementService {

    /**
     * 新增
     */
    R insert(CabDeviceAdvertisementInsertDto dto);

    /**
     * 删除
     */
    R delete(int id);

    /**
     * 更新
     */
    R update(CabDeviceAdvertisementUpdateDto dto);

    /**
     * 根据主键 id 查询
     */
    R load(int id);

    /**
     * 查询该用户的所有广告信息
     * @param request
     * @return
     */
    R findUserByIdAll(HttpServletRequest request);

    /**
     * 分页查询
     */
//    Map<String,Object> pageList(int offset, int pagesize);




    /**
     * 修改expirationTime
     */
}
