package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import java.util.Date;

@Data
public class CabDelivery implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * id
   */
  private Integer id;

  /**
   * 工号
   */
  private String workid;

  /**
   * 公司编号
   */
  private Integer companyid;

  /**
   * 快递员银行账户
   */
  private String accountid;

  public CabDelivery() {
  }

}
