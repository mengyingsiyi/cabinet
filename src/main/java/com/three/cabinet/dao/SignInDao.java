package com.three.cabinet.dao;

import com.three.cabinet.dto.SignInDto;
import com.three.cabinet.entity.CabSignIn;

public interface SignInDao {

    //签到
    boolean insertSignIn(CabSignIn signIn);

    //修改
    boolean updateSignIn(SignInDto signIn);

    //查询签到详情
    SignInDto selectSignIn(int uid);

}
