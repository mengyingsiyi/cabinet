package com.three.cabinet.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @program: sasigay
 * @description:    手动创建的，部分没有测试不能保证成功
 * @author: ztk
 * @create: 2020-08-21 20:31
 **/
public class JacksonUtil {
    public static ObjectMapper mapper = new ObjectMapper();

    // json转实体类
    public static<T> T json2Object(String json, Class<T> clz) throws IOException {
        byte[] bytes = json.getBytes("UTF-8");
        T t = mapper.readValue(bytes, clz);
        return t;
    }

    // json数组转List
    public static <T> List<T> jsonArray2List(String jsonArray) throws JsonProcessingException {
        List<T> list = mapper.readValue(jsonArray, List.class);
        return list;
    }

    // json转Map
    public static Map<String, Object> json2Map(String json) throws JsonProcessingException {
        Map map = mapper.readValue(json, Map.class);
        return map;
    }

    // json转JsonNode
    public static JsonNode json2JosnNode(String json) {
        // 这两个也行
        // mapper.readValue(json, JsonNode.class);
        // mapper.readTree(json);
        JsonNode jsonNode = mapper.valueToTree(json);
        return jsonNode;
    }

    // 实体类转JsonNode(不出意外的话List,Map也可以)
    public static <T> JsonNode Object2JsonNode(T t) {
        return mapper.valueToTree(t);
    }

    // JsonNode转实体类
    public static <T> T JsonNode2Object(JsonNode jsonNode, Class<T> clz) throws JsonProcessingException {
        T t = mapper.treeToValue(jsonNode, clz);
        return t;
    }

    // Object转json
    public static <T> String Object2Json(T t) throws JsonProcessingException {
        return mapper.writeValueAsString(t);
    }
}
