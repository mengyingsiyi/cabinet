package com.three.cabinet.interceptor;

import com.three.cabinet.config.RedisKeyConfig;
import com.three.cabinet.constant.LoginConstant;
import com.three.cabinet.constant.SystemConstant;
import com.three.cabinet.dto.LoginDto;
import com.three.cabinet.entity.CabUser;
import com.three.cabinet.exception.UserException;
import com.three.cabinet.note.CheckToken;
import com.three.cabinet.service.LoginService;
import com.three.cabinet.util.IpUtil;
import com.three.cabinet.util.JacksonUtil;
import com.three.cabinet.util.JedisCore;
import com.three.cabinet.util.JwtUtil;
import com.three.cabinet.vo.R;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.logging.Handler;

/**
 * 校验令牌是否有效
 */
@Component
public class TokenInterceptor implements HandlerInterceptor {
    @Autowired
    private JedisCore jedisCore;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        //检查是否有CheckToken注释，有没则跳过认证
        if (method.isAnnotationPresent(CheckToken.class)) {
            CheckToken checkToken = method.getAnnotation(CheckToken.class);
            if (checkToken.required()) {
                String userToken = request.getHeader(LoginConstant.ACCESS_TOKEN);
                if (!StringUtils.isEmpty(userToken)) {
                    String userJson = null;
                    boolean mark = false;
                    CabUser user = null;
                    try {
                        userJson = JwtUtil.parseJWT(userToken);
                        userJson = jedisCore.get(userJson);
                        user = JacksonUtil.json2Object(userJson, CabUser.class);
                        if (StringUtils.isEmpty(jedisCore.get(LoginConstant.USER_TOKEN + IpUtil.getIPAddress(request) + user.getTelphone()))) {
                            // 如果CabUser表中加一个上次登录的ip地址字段，就可以查出来ip
                            // 再调一下第三方接口，查一下ip地址所在地
                            // 不过这个表大家都在用，太麻烦了，就不加了
                            throw new UserException("该账号在另一地址登录，请重新登录!");
                        }
                    } catch (ExpiredJwtException e) {
                        mark = true;
                    }
                    if (mark) {
                        String refreshToken = request.getHeader(LoginConstant.REFRESH_TOKEN);
                        try {
                            userJson = JwtUtil.parseJWT(refreshToken);
                            userJson = jedisCore.get(userJson);
                            userJson = jedisCore.get(userJson);
                            user = JacksonUtil.json2Object(userJson, CabUser.class);
                        } catch (ExpiredJwtException e) {
                            throw new UserException("需要重新登录!");
                        }
                    }

                    request.setAttribute("userId", user.getId());
                    request.setAttribute("userInfo", user);
                    return true;
                } else {
                    throw new UserException("没有登录令牌！");
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}