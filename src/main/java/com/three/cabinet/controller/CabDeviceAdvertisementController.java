package com.three.cabinet.controller;

import com.three.cabinet.dto.CabDeviceAdvertisementInsertDto;
import com.three.cabinet.dto.CabDeviceAdvertisementUpdateDto;
import com.three.cabinet.entity.CabDeviceAdvertisement;
import com.three.cabinet.service.CabDeviceAdvertisementService;
import com.three.cabinet.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 设备广告信息
 * @author: miao
 * @date: 2020/8/20
 */
@Api(tags = "设备广告信息")
@RestController
@RequestMapping("/api/cabinet/devicead")
public class CabDeviceAdvertisementController {
    @Autowired
    private CabDeviceAdvertisementService service;


    /**
     * 新增
     */
    @ApiOperation("新增")
    @PostMapping("insert.do")
    public R insert(CabDeviceAdvertisementInsertDto dto) {
        return service.insert(dto);
    }

    /**
     * 删除
     */
    @ApiOperation("删除")
    @GetMapping("delete.do")
    public R delete(int id) {
        return service.delete(id);
    }

    /**
     * 更新
     */
    @ApiOperation("修改")
    @PostMapping("update.do")
    public R update(CabDeviceAdvertisementUpdateDto dto) {
        return service.update(dto);
    }

    /**
     * 根据主键 id 查询
     */
    @ApiOperation("根据主键 id 查询")
    @GetMapping("load.do")
    public R load(int id) {
        return service.load(id);
    }

    /**
     * 查询该用户的所有广告信息
     * @param request
     * @return
     */
    @ApiOperation("查询用户的所有广告信息")
    @GetMapping("findUserByIdAll")
    public R findUserByIdAll(HttpServletRequest request) {
        return service.findUserByIdAll(request);
    }
}
