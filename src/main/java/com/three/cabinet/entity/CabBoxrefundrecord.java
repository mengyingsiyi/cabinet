package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import java.util.Date;

@Data
public class CabBoxrefundrecord implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * nn退款记录id
   */
  private Integer brrid;

  /**
   * 退款账户
   */
  private String bacc;

  /**
   * nn退款原因
   */
  private String refundreason;

  /**
   * 添加时间
   */
  private Date addtime;

  /**
   * nn更新时间
   */
  private Date updatetime;

  public CabBoxrefundrecord() {
  }

}
