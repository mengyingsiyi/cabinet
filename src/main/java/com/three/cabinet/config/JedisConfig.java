package com.three.cabinet.config;

import com.three.cabinet.util.JedisCore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JedisConfig {
    @Value("${cabinet.redis.host}")
    private String host;
    @Value("${cabinet.redis.port}")
    private int port;
    @Value("${cabinet.redis.pass}")
    private String pass;

    @Bean
    public JedisCore createJC(){

        return new JedisCore(host,port,pass);
    }
}
