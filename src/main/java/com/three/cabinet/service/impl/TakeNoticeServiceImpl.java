package com.three.cabinet.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.three.cabinet.config.JedisConfig;
import com.three.cabinet.config.RedisKeyConfig;
import com.three.cabinet.configuration.DelaySender;
import com.three.cabinet.dao.TakeNoticeDao;
import com.three.cabinet.dto.GetNoticeDto;
import com.three.cabinet.dto.SendNoticeDto;
import com.three.cabinet.service.TakeNoticeService;
import com.three.cabinet.util.JedisCore;
import com.three.cabinet.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * @Package: com.three.cabinet.service.impl
 * @ClassName: TakeNoticeServiceImpl
 * @Author: yan
 * @Description:
 */
@Service
public class TakeNoticeServiceImpl implements TakeNoticeService {
    @Autowired
    private JedisCore jedisCore;
    @Autowired
    private TakeNoticeDao takeNoticeDao;
    //死信队列
    @Autowired
    private DelaySender delaySender;

    @Override
    public R sendNotice(GetNoticeDto getNoticeDto) {
        //获取随机验证码
        int code = code(6);
        //通过设备id获取设备名称
        String name = takeNoticeDao.getDeviceById(getNoticeDto.getDeviceId());
        String notice = "您的快递已经到达"+name+"取件码为："+code+"，存放时间超过一定将产生费用,请尽快提取。";
        int addNotice = takeNoticeDao.addNotice(notice, name, code, getNoticeDto.getTelephone());
        if (addNotice!=0){
            //放在redis里，减少查询
            SendNoticeDto sendNoticeDto = new SendNoticeDto(notice, name, code, getNoticeDto.getTelephone());

            //转化为json放入redis
            Object json = JSONArray.toJSON(getNoticeDto);
            jedisCore.set(RedisKeyConfig.NOTICE_CODE+getNoticeDto.getDeviceId()+":"+addNotice,json.toString(),RedisKeyConfig.NOTICECODE_TIME);

            delaySender.sendDelay(sendNoticeDto);
            return R.ok(sendNoticeDto,"短信内容");
        } else {
            return R.error("发送失败");
        }
    }

    @Override
    public R againSendNotice(SendNoticeDto sendNoticeDto) {
        String s = jedisCore.get(sendNoticeDto.getNcid() + ":" + sendNoticeDto.getVerificationCode());
        //GetNoticeDto getNoticeDto = JSONArray.parseObject(s);

        //通过反射 获取需要转换对象的类型
        GetNoticeDto getNoticeDto = JSONArray.parseObject(s,GetNoticeDto.class);

        //通过设备id获取设备名称
        String name = takeNoticeDao.getDeviceById(getNoticeDto.getDeviceId());
        String notice = "您的快递已经到达"+name+"，已经为您保存一天时间，请尽快取出。取件码为："+sendNoticeDto.getVerificationCode();

        sendNoticeDto.setContent(notice);
        return R.ok(sendNoticeDto,"再次的短信");
    }

    //生成随机验证码
    private int code(int num){
        //1000  100
        Random random = new Random();
        int max = (int) (Math.pow(10,num)-Math.pow(10,num-1));
        return (int) (random.nextInt(max)+Math.pow(10,num-1));
    }
}
