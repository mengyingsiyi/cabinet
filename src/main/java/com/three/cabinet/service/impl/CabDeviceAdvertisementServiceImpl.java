package com.three.cabinet.service.impl;

import com.three.cabinet.config.RedisKeyConfig;
import com.three.cabinet.dao.CabDeviceAdvertisementDao;
import com.three.cabinet.dto.CabDeviceAdvertisementInsertDto;
import com.three.cabinet.dto.CabDeviceAdvertisementUpdateDto;
import com.three.cabinet.entity.CabDeviceAdvertisement;
import com.three.cabinet.note.CheckToken;
import com.three.cabinet.service.CabDeviceAdvertisementService;
import com.three.cabinet.util.JedisCore;
import com.three.cabinet.util.TokenUtil;
import com.three.cabinet.vo.R;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 设备广告信息
 * @author: miao
 * @date: 2020/8/20
 */
@Service
public class CabDeviceAdvertisementServiceImpl implements CabDeviceAdvertisementService {
    @Autowired
    private CabDeviceAdvertisementDao dao;
    @Autowired
    private JedisCore jedisCore;



    @Override
    @CheckToken
    public R insert(CabDeviceAdvertisementInsertDto dto) {

                //新增设备则免费试用一周
                jedisCore.set(RedisKeyConfig.CD_TIME+ dto.getAdId(),"",RedisKeyConfig.CD_Trial_TIME);
                CabDeviceAdvertisement cd = new CabDeviceAdvertisement();
                cd.setGroupId(dto.getGroupId());
                cd.setAdId(dto.getAdId());

                int s = (int) jedisCore.ttl(RedisKeyConfig.CD_TIME + dto.getAdId());
                cd.setExpirationTime(s);

                int add = dao.add(cd);
                if (add > 0) {
                    return R.ok();
                } else {
                    return R.error("新增失败");
                }

    }

    @Override
    @CheckToken
    public R delete(int id) {
        dao.delete(id);
        return R.ok();
    }

    /**
     * 此处用redis
     * @param dto
     * @return
     */
    @Override
    @CheckToken
    public R update(CabDeviceAdvertisementUpdateDto dto) {

//            if (dto.getState().equals("s")){
//                System.err.println("++++++++++++++++已禁用");
//                return R.error("该状态已被禁用");
//            }else
                if (!jedisCore.checkKey(RedisKeyConfig.CD_TIME+dto.getAdId())){
                return R.error("该广告状态已被禁用");
           }else {
                //判断该广告时间是否超时 超时则修改状态
                long ttl = jedisCore.ttl(RedisKeyConfig.CD_TIME + dto.getAdId());
                System.err.println(ttl);
                if (ttl < 0) {//会员时间是否超时
                    //已超时
                    //更改状态 a打开, s禁用
                    dao.updateState("s");

                    jedisCore.del(RedisKeyConfig.CD_TIME+dto.getAdId());
                    System.err.println("超时, 已被禁用----------");
                    return R.error("超时, 已被禁用");
                } else {
                    //未超时
                    CabDeviceAdvertisement cd = new CabDeviceAdvertisement();
                    cd.setGroupId(dto.getGroupId());

                    cd.setAdId(dto.getAdId());
                    cd.setState("a");
                    cd.setExpirationTime((int) ttl);


                    int update = dao.update(cd);
                    if (update > 0) {
                        return R.ok();
                    } else {
                        return R.error("修改失败");
                    }
                }


            }

    }

    @Override
    @CheckToken
    public R load(int id) {
        CabDeviceAdvertisement byId = dao.findById(id);
        if (byId != null) {
            return R.ok(byId);
        }
        return R.error("未找到");
    }


    /**
     * 查询该用户的所有广告信息
     * @param request
     * @return
     */
    @Override
    @CheckToken
    public R findUserByIdAll(HttpServletRequest request) {
        Object userId = request.getSession().getAttribute("userId");
        if (userId == null) {
            return R.error("userId 为空");
        } else {

            //获取user名称

            List<CabDeviceAdvertisement> userByIdAll = dao.findUserByIdAll((int)userId);
            for (CabDeviceAdvertisement a : userByIdAll) {
                System.out.println(a);
            }
            if (null != userByIdAll) {
                return R.ok(userByIdAll);
            }
            return R.error("未找到");
        }

    }
}
