package com.three.cabinet.util;

import java.util.UUID;

/**
 * @program: cabinet
 * @Author ywb(余文冰)
 * @Description: 生成32位随机字符串
 * @Date: Create in 22:27 2020/8/20 
 * @Version: 1.0
 */

public class UUIDUtil {
    public static String createUUID(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }
}
