package com.three.cabinet.test;


import com.three.cabinet.Enum.AccountDetailFlag;
import com.three.cabinet.controller.PayController;
import com.three.cabinet.dao.PayDao;
import com.three.cabinet.dto.AccountDetailDto;
import com.three.cabinet.dto.PayDto;
import com.three.cabinet.util.EncryptUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Pass_Test {
    @Autowired
    private PayDao dao;
    @Autowired
    private PayController controller;

    @Test
    public void t1() {
        String key = EncryptUtil.createAESKey();
        System.out.println(key);
        String pass = "123456";
        String vm = EncryptUtil.aesenc(key, pass);
        System.out.println("密文：" + vm);
        System.out.println("明文：" + EncryptUtil.aesdec(key, vm));
    }

    @Test
    public void t2() {
//        dao.editMoney(10,1);
        dao.changeLog(1, 10, AccountDetailFlag.充值);
    }

    @Test
    public void t3() {
        PayDto payDto = new PayDto();
        payDto.setOid("111");
        payDto.setPaytype(1);
        payDto.setPrice(1);
        payDto.setOrderdes("这不是商品");
        controller.createPay(payDto);
    }

}
