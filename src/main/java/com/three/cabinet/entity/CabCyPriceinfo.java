package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import java.util.Date;


@Data
public class CabCyPriceinfo implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 柜子id
   */
  private Integer pdid;

  /**
   * 使用费用(分)
   */
  private Integer price;

  /**
   * 箱子规格 big表示大；mid表示中；small表示小
   */
  private String typeno;

  /**
   * n表示正常费用（normal） e表示超时费用（expire）
   */
  private String pricetype;

  /**
   * 超时时间 1，十二个小时以内 2二十四个小时 3超过二十四小时
   */
  private Integer timeout;

  /**
   * 添加时间
   */
  private Date addtime;

  /**
   * 更新时间
   */
  private Date updatetime;

  public CabCyPriceinfo() {
  }

}
