package com.three.cabinet.util;

import com.three.cabinet.dao.SignInDao;
import com.three.cabinet.dto.SignInDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DateQuartz {
    @Autowired
    private  SignInDao dao;

    @Scheduled(cron="* * * 1 * ? ")
    public  boolean resetDetail(){
        SignInDto dto = new SignInDto();
        dto.setDetail(null);
        boolean flag = dao.updateSignIn(dto);
        return flag;
    }
}
