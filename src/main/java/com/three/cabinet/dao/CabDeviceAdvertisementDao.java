package com.three.cabinet.dao;

import com.three.cabinet.dto.UserCabSelectAllDto;
import com.three.cabinet.entity.CabDeviceAdvertisement;

import java.util.List;

/**
 * 设备广告信息
 */
public interface CabDeviceAdvertisementDao {
    int add(CabDeviceAdvertisement cabDeviceAdvertisement);

    int update(CabDeviceAdvertisement cabDeviceAdvertisement);

    int delete(int id);

    CabDeviceAdvertisement findById(int id);

    //更改订单状态
    int updateState(String state);

    List<CabDeviceAdvertisement> findUserByIdAll(int uid);
}
