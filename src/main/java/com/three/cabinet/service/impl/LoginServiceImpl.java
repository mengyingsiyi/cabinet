package com.three.cabinet.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.three.cabinet.constant.LoginConstant;
import com.three.cabinet.dao.LoginDao;
import com.three.cabinet.dto.LoginDto;
import com.three.cabinet.entity.CabUser;
import com.three.cabinet.service.LoginService;
import com.three.cabinet.util.IpUtil;
import com.three.cabinet.util.JacksonUtil;
import com.three.cabinet.util.JedisCore;
import com.three.cabinet.util.JwtUtil;
import com.three.cabinet.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @program: cabinet
 * @description:
 * @author: ztk
 * @create: 2020-08-21 00:03
 **/
@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private LoginDao loginDao;

    @Autowired
    private JedisCore jedisCore;

    @Override
    public CabUser findUser(LoginDto dto) {
        return loginDao.findUser(dto);
    }

    @Override
    public CabUser findUserByTelPhone(String telPhone) {
        return loginDao.findUserByTelPhone(telPhone);
    }

    /**
     * 账号密码登录
     * @param dto
     * @param request
     * @return
     */
    @Override
    public R loginInDevice(LoginDto dto, HttpServletRequest request) {
        // 请求的ip地址
        String ipAddress = IpUtil.getIPAddress(request);
        String userTokenKey = LoginConstant.USER_TOKEN
                + "*" + ":" + dto.getTelPhone();
        String userTokenValue = null;
        CabUser user = null;

        // 从redis里获取用户信息
        Set<String> keys = jedisCore.getKeys(userTokenKey);

        if (keys == null || keys.size() == 0) {
            // 未登录过
            user = findUser(dto);
            if (user != null) {
                userTokenKey = LoginConstant.USER_TOKEN + ipAddress + ":" + dto.getTelPhone();
                try {
                    userTokenValue = JacksonUtil.Object2Json(user);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                // 存入Redis，有效期-1
                jedisCore.set(userTokenKey, userTokenValue, LoginConstant.USER_TOKEN_TIME);
                return R.ok(packingSendMsg(LoginConstant.USER_NORMAL_LOGIN_CODE, userTokenKey, userTokenValue));
            }
        } else {
            // 登录过
            String userJson = null;
            for (String key : keys) {
                if (!StringUtils.isEmpty(key)) {
                    userJson = key;
                    break;
                }
            }
            userJson = jedisCore.get(userJson);
            try {
                user = JacksonUtil.json2Object(userJson, CabUser.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            userTokenKey = LoginConstant.USER_TOKEN + ipAddress + ":" + dto.getTelPhone();

            if (!StringUtils.isEmpty(jedisCore.get(userTokenKey))) {
                // 给上一个登录的挤掉
                jedisCore.delAllKeyFromKeysSelect(LoginConstant.USER_TOKEN + "*" + user.getTelphone());
                // 存入新的，有效期-1
                jedisCore.set(userTokenKey, userJson, LoginConstant.USER_TOKEN_TIME);
                return R.ok(packingSendMsg(LoginConstant.USER_ALREADY_LOGIN_CODE, userTokenKey, userJson));
            }
        }
        return R.error("账号或者密码错误！");
    }

    /**
     * 二维码登录快递柜，仅快递员
     * @param telPhone
     * @return
     */
    @Override
    public R loginByQRInCabinet(String telPhone) {
        // 在快递柜登录能干什么？
        // 1. 取快递员对应的快递公司的所有快递
        // 2. 存快递（快件条形码一扫就下放入快递柜的订单）
        // 所以用不用给令牌？
        // 还是给吧，可以设定jwt有效期短一点比如说15min
        // 然后有了token才可以请求上面的两个接口

        String userTokenKey = LoginConstant.COURIER_TOKEN + telPhone;

        CabUser user = null;
        // 从redis里获取用户信息
        String userTokenValue = jedisCore.get(userTokenKey);

        if (StringUtils.isEmpty(userTokenValue)) {
            // 未登录过
            user = findUserByTelPhone(telPhone);
            if (user != null && user.getRoleid() == 2) {
                try {
                    userTokenValue = JacksonUtil.Object2Json(user);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            } else {
                return R.error("不是快递员！");
            }
        } else {
            // 登录过
            userTokenKey = LoginConstant.COURIER_TOKEN + telPhone;
        }
        // 存入Redis，有效期-1
        jedisCore.set(userTokenKey, userTokenValue, LoginConstant.USER_TOKEN_TIME);
        return R.ok(packingSendMsg(LoginConstant.COURIER_LOGIN_CODE, userTokenValue));
    }

    @Override
    public R loginByQRInPC(String telPhone, HttpServletRequest request) {
        // 不想写了
        // 快递员需要一套新的token名称
        // 然后和上面的基本一样
        // 只不过用的是不同的查询用户的方法
        // telephone number是唯一的
        return null;
    }

    @Override
    public R loginByTelMsg() {
        // 有点没想通，改日再写
        return null;
    }

    /**
     * 包装code，access_token和refresh_token
     * @param loginCode
     * @param userTokenKey
     * @param userTokenValue
     * @return
     */
    private Map<String, String> packingSendMsg(String loginCode, String userTokenKey, String userTokenValue) {
        // 用来存放登录信息
        Map<String, String> map = new HashMap<>();
        // 存入登录结果代码
        map.put("code", loginCode);
        Date date = new Date();
        long now = date.getTime();
        date.setTime(now + LoginConstant.ACCESS_TOKEN_TIME);
        // 存入登录令牌
        map.put(LoginConstant.ACCESS_TOKEN, JwtUtil.createJWT(userTokenValue, date));
        date.setTime(now + LoginConstant.REFRESH_TOKEN_TIME);
        // 存入刷新令牌
        map.put(LoginConstant.REFRESH_TOKEN, JwtUtil.createJWT(userTokenKey, date));

        return map;
    }

    private Map<String, String> packingSendMsg(String loginCode, String userTokenValue) {
        // 用来存放登录信息
        Map<String, String> map = new HashMap<>();
        // 存入登录结果代码
        map.put("code", loginCode);
        Date date = new Date();
        long now = date.getTime();
        date.setTime(now + LoginConstant.COURIER_ACCESS_TOKEN_TIME);
        // 存入登录令牌
        map.put(LoginConstant.COURIER_ACCESS_TOKEN, JwtUtil.createJWT(userTokenValue, date));

        return map;
    }
}
