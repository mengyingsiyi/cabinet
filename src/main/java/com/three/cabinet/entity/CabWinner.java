package com.three.cabinet.entity;

import lombok.Data;

/**
 * @program: cabinet
 * @Author ywb(余文冰)
 * @Description: 中奖的记录日志
 * @Date: Create in 20:15 2020/8/22 
 * @Version: 1.0
 */
@Data
public class CabWinner {
    private Integer uid;//用户id
    private String productName;
    private String oid;//流水号
}
