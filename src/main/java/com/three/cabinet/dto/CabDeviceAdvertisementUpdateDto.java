package com.three.cabinet.dto;

import lombok.Data;

@Data
public class CabDeviceAdvertisementUpdateDto {

    /**
     * 柜组id
     */
    private int groupId;
    /**
     * 广告id
     */
    private int adId;



}
