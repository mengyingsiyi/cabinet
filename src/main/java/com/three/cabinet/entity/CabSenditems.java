package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import java.util.Date;

@Data
public class CabSenditems implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * id
   */
  private Integer id;

  /**
   * 发件地址
   */
  private Integer sdaid;

  /**
   * 收件地址id
   */
  private Integer rdaid;

  /**
   * 付款方式 tp:到付；sp：寄付
   */
  private String payclass;

  /**
   * 物品数量
   */
  private Integer goodscount;

  /**
   * 备注
   */
  private String memo;

  /**
   * 寄件码
   */
  private String sendcode;

  /**
   * 收件码
   */
  private String takecode;

  /**
   * 快递费用编号
   */
  private Integer cfid;

  /**
   * 箱子编号
   */
  private Integer num;

  /**
   * 柜子id
   */
  private Integer did;

  /**
   * 快递单号
   */
  private String tracknum;

  /**
   * 快递员id
   */
  private Integer courierid;

  /**
   * 订单号
   */
  private String ordernum;

  /**
   * 设备收费类型 ; 付款方式;zfb表示支付宝，wx表示微信，pp表示paypal，cash表示现金
   */
  private String payment;

  /**
   * 用户第三方id appid
   */
  private Integer appid;

  /**
   * 用户第三方 openid
   */
  private Integer openid;

  /**
   * 第三方交易订单
   */
  private String transactionid;

  /**
   * 转账状态
   */
  private String transferstatus;

  /**
   * 订单状态 ps:付款完成；fp:待取货；js:结束；暂不用ys:运送；fg:待收货
   */
  private String orderstate;

  /**
   * 添加时间
   */
  private Date addtime;

  /**
   * 更新时间
   */
  private Date updatetime;

  public CabSenditems() {
  }

}
