package com.three.cabinet.interceptor;

import com.three.cabinet.entity.CabUser;
import com.three.cabinet.note.CheckToken;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @program: cabinet
 * @description:
 * @author: ztk
 * @create: 2020-08-21 17:44
 **/
@Component
public class AuthorizationInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        String[] path = requestURI.split("/");
        if (path.length >= 2) {
            CabUser user = (CabUser) request.getAttribute("userInfo");
            if (user != null) {
                // 普通用户不用校验
                if (user.getRoleid() == 2) {
                    // 快递员用户
                    if (checkApiPathHead(path) && path[2].equals("ad")) {

                        return true;
                    }
                } else if (user.getRoleid() == 3) {
                    // 广告用户
                    if (checkApiPathHead(path) && path[2].equals("ad")) {
                        return true;
                    }
                }
            }
            return true;
        } else {
            return true;
        }
    }
    public boolean checkApiPathHead(String[] path) {
        return path[0].equals("api") && path[1].equals("cabinet") ? true : false;
    }
}
