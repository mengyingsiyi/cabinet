package com.three.cabinet.util;

import java.time.LocalDate;
import java.util.UUID;

import static com.three.cabinet.util.DateUtil.formatDate;

/**
 * 用于生成令牌
 */
public class TokenUtil {
    /**
     * 用于生成一个令牌，保证唯一性
     * @param uid
     * @return
     */
    public static String createToken(int uid){
        return UUID.randomUUID().toString().replaceAll("-","")+"_"+uid;
    }

    /**
     * 生成签到令牌
     * @param uid
     * @param date
     * @return
     */
    public static String buildSignKey(int uid, LocalDate date){
        return String.format("sign:%d:%s",uid,formatDate(date));
    }
}
