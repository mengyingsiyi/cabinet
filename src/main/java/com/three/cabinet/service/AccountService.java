package com.three.cabinet.service;

import com.three.cabinet.vo.R;

/**
 * @program: cabinet
 * @Author
 * @Description:
 * @Date: Create in 18:03 2020/8/20
 * @Version: 1.0
 */

public interface AccountService {
    R addBalance(int money,int type,Integer uid);

    R lotto(Integer userId);
}
