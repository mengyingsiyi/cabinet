package com.three.cabinet.config;

/**
 * 设置登录令牌在redis中存储的有效期
 */
public class RedisKeyConfig {
    //令牌
    public static final String PHONE_TOKEN="cabinet:phone:";//后面追加手机号 值存储令牌
    public static final String TOKEN_USER="cabinet:token:";//后面追加令牌 值存储用户信息

    //
    public static final String PHONE_FOR="cabinet:for:phone:";//后面跟手机号 冻结的账号
    //记录登录输错的手机号，在10分钟内有效期，只要次数超过三次name就会将手机号进行封号处理
    public static final String PHONE_ERROR="cabinet:error:phone:";//后面跟手机号和时间戳

    //用户账户充值的金额
    public static final String ACCOUNTADDMOEY = "cabinet:accmoney:";
//    public static final String ACCOUNTPAYTYPE = "cabinet:accpaytype:";

    //有效期
    public static final int TOKEN_TIME=30*60*60; //令牌有效期 单位秒
    public static final int TOKENFOR_TIME=30*60; //冻结账号 单位秒
    public static final int PHONERROR_TIME=10*60; //错误的有效期 单位秒
    public static final int ACCOUNTADDMOEY_TIME=60*60*2; //用户账户充值金额的有效期 单位秒
    public static final int ACCOUNTPAYTYPE_TIME=60*60*2; //用户账户充值方式的有效期 单位秒
    public static final int NOTICECODE_TIME=60*60*24; //验证码的有效期 单位秒


    //通知相关
    public static final String NOTICE_CODE="cabinet:notice:code:";//后跟用户手机号和验证码，记录取件信息

    //设备广告
    public static final String CD_TIME = "cabinet:cd:time:";//后面追加广告id

    public static final int CD_Trial_TIME= 60*60*24*7;//试用时间为7天
    public static final int CD_VIP_TIME= 60*60*24*30;//充值 30天

}
