package com.three.cabinet.constant;

public class SystemConstant {
    // 令牌的请求消息头
    public static final String TOKEN_HEADER="ACCESS_TOKEN";

    //设置OSS存储空间名称
    public static final String OSS_BUCKET="cabinet03";

    //用户组
    public static final String GROUP_ID="user";
}
