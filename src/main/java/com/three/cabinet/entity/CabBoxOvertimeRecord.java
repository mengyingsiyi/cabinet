package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;
import java.util.Date;

@Data
public class CabBoxOvertimeRecord implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 记录编号
   */
  private Integer orderid;

  /**
   * 箱子id
   */
  private Integer boxid;

  /**
   * 柜子id
   */
  private Integer deviceid;

  /**
   * 超时费用 单位：分
   */
  private Integer price;

  /**
   * 设备收费类型：zfb表示支付宝，wx表示微信
   */
  private String payment;

  /**
   * 超时时间 单位：分钟
   */
  private Integer duration;

  /**
   * 添加时间
   */
  private Date addtime;

  public CabBoxOvertimeRecord() {
  }

}
