package com.three.cabinet.controller;


import com.three.cabinet.dto.CabAdvertisementUpdateDto;
import com.three.cabinet.entity.CabAdvertisement;
import com.three.cabinet.service.CabAdvertisementService;
import com.three.cabinet.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;


/**
 * 广告信息
 * @author: miao
 * @date: 2020/8/20
 */
@Api(tags = "广告信息")
@RestController
@RequestMapping("/api/cabinet/ad/")
public class CabAdvertisementController {

    @Autowired
    private CabAdvertisementService service;



    /**
     * 删除
     */
    @ApiOperation("删除")
    @GetMapping("delete.do")
    public R delete(@RequestParam int id) {
        return service.delete(id);

    }

    /**
     * 更新
     */
    @ApiOperation("修改")
    @PostMapping("update.do")
    public R update(@RequestBody CabAdvertisementUpdateDto dto, HttpServletRequest request) {

        return service.update(dto,request);
    }

    /**
     * 根据主键 id 查询
     */
    @ApiOperation("根据id查询")
    @GetMapping("load")
    public R load(@RequestParam int id) {
        return service.load(id);
    }

    @ApiOperation("上传视频")
    @PostMapping("upload")
    public R upload(MultipartFile file, @RequestParam String brief,HttpServletRequest request) {

        return service.upload(file, brief,request);
    }

}
