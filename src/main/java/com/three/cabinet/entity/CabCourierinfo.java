package com.three.cabinet.entity;

import java.io.Serializable;
import lombok.Data;

@Data
public class CabCourierinfo implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * ciid
   */
  private Integer ciid;

  /**
   * 用户id
   */
  private Integer userid;

  /**
   * 账号状态 a表示启用；s表示禁用
   */
  private String statusno;

  /**
   * 公司id
   */
  private Integer comid;

  /**
   * 工号
   */
  private String worknum;

  public CabCourierinfo() {
  }

}
