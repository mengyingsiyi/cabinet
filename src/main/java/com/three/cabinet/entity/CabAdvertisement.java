package com.three.cabinet.entity;
import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import java.util.List;

@Data
public class CabAdvertisement implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * ad_id
   */
  private Integer adId;

  /**
   * 用户id
   */
  private Integer userId;

  /**
   * 视频名称
   */
  private String videoName;

  /**
   * 视频简介
   */
  private String videoIntroduce;

  /**
   * 视频地址
   */
  private String videoUrl;

  /**
   * 添加时间
   */
  private Date addTime;

  /**
   * 更新时间
   */
  private Date updateTime;

  private CabUser cabUser;
  public CabAdvertisement() {
  }

}
