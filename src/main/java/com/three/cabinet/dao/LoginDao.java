package com.three.cabinet.dao;

import com.three.cabinet.dto.LoginDto;
import com.three.cabinet.entity.CabUser;

/**
 * @program: cabinet
 * @description:
 * @author: ztk
 * @create: 2020-08-21 00:02
 **/
public interface LoginDao {
    // 登录校验
    CabUser findUser(LoginDto dto);

    CabUser findUserByTelPhone(String telPhone);
}
