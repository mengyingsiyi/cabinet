package com.three.cabinet.dao;

import com.three.cabinet.Enum.AccountDetailFlag;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

/**
 * @program: cabinet
 * @Author ywb(余文冰)
 * @Description: 
 * @Date: Create in 15:18 2020/8/20 
 * @Version: 1.0
 */
@Repository
public interface PayDao {

    //充值
    @Update("update cab_wallet set income = #{money},balance = balance + #{money} where id = #{uid}")
    int editMoney(@Param("money") Double money, @Param("uid")int uid);

    //添加账户变动
    @Insert("insert into cab_accountdetail(uid,atime,money,type) values(#{uid},now(),#{money},#{t})")
    int changeLog(@Param("uid") int uid, @Param("money") double money, AccountDetailFlag t);
}
